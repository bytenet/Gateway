using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace UMC.Proxy
{
    public class Certificater
    {
        static ConcurrentDictionary<string, int> _Hosts = new ConcurrentDictionary<string, int>();

        public static ConcurrentDictionary<string, int> UnCertificatesHosts
        {
            get
            {
                return _Hosts;
            }
        }
        public static Dictionary<string, Certificater> Certificates
        {
            get
            {
                return _certificates;
            }
        }
        static Dictionary<string, Certificater> _certificates = new Dictionary<string, Certificater>();

        public string Name
        {
            get; set;
        }

        public X509Certificate2 Certificate
        {
            get; set;
        }
        public int Time
        {
            get; set;
        }
    }
}