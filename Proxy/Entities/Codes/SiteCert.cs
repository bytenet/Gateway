using System;
using System.Collections.Generic;
using UMC.Data;
namespace UMC.Proxy.Entities
{
    public partial class SiteCert
    {
        readonly static Action<SiteCert, object>[] _SetValues = new Action<SiteCert, object>[] { (r, t) => r.CheckTime = Reflection.ParseObject(t, r.CheckTime), (r, t) => r.Domain = Reflection.ParseObject(t, r.Domain), (r, t) => r.ExpirationTime = Reflection.ParseObject(t, r.ExpirationTime), (r, t) => r.IsApiumc = Reflection.ParseObject(t, r.IsApiumc), (r, t) => r.PrivateKey = Reflection.ParseObject(t, r.PrivateKey), (r, t) => r.PublicKey = Reflection.ParseObject(t, r.PublicKey), (r, t) => r.SubDomain = Reflection.ParseObject(t, r.SubDomain) };
        readonly static string[] _Columns = new string[] { "CheckTime", "Domain", "ExpirationTime", "IsApiumc", "PrivateKey", "PublicKey", "SubDomain" };
        protected override void SetValue(string name, object obv)
        {
            var index = Utility.Search(_Columns, name, StringComparer.CurrentCultureIgnoreCase);
            if (index > -1) _SetValues[index](this, obv);
        }
        protected override void GetValues(Action<String, object> action)
        {
            AppendValue(action, "CheckTime", this.CheckTime);
            AppendValue(action, "Domain", this.Domain);
            AppendValue(action, "ExpirationTime", this.ExpirationTime);
            AppendValue(action, "IsApiumc", this.IsApiumc);
            AppendValue(action, "PrivateKey", this.PrivateKey);
            AppendValue(action, "PublicKey", this.PublicKey);
            AppendValue(action, "SubDomain", this.SubDomain);
        }

        protected override RecordColumn[] GetColumns()
        {
            var cols = new RecordColumn[7];
            cols[0] = RecordColumn.Column("CheckTime", this.CheckTime);
            cols[1] = RecordColumn.Column("Domain", this.Domain);
            cols[2] = RecordColumn.Column("ExpirationTime", this.ExpirationTime);
            cols[3] = RecordColumn.Column("IsApiumc", this.IsApiumc);
            cols[4] = RecordColumn.Column("PrivateKey", this.PrivateKey);
            cols[5] = RecordColumn.Column("PublicKey", this.PublicKey);
            cols[6] = RecordColumn.Column("SubDomain", this.SubDomain);
            return cols;
        }

    }
}

