
using UMC.Data;

namespace UMC.Proxy.Entities;

public partial class SiteCert : Record
{
    public string Domain { get; set; }
    public string PublicKey { get; set; }
    public string PrivateKey { get; set; }

    public int? ExpirationTime
    {
        get; set;
    }
    public int? CheckTime
    {
        get; set;
    }
    public bool? IsApiumc
    {
        get; set;
    }


    public string SubDomain
    {
        get; set;
    }

}
