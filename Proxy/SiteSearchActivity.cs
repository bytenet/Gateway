using System;
using System.Collections.Generic;
using System.Linq;
using UMC.Web;
using UMC.Data.Entities;
using UMC.Proxy.Entities;
using System.IO;
using UMC.Data;
using System.Text.RegularExpressions;
using UMC.Web.UI;

namespace UMC.Proxy.Activities
{
    [UMC.Web.Mapping("Proxy", "Search", Auth = WebAuthType.User)]
    public class SiteSearchActivity : UMC.Web.WebActivity
    {
        public override void ProcessActivity(WebRequest request, WebResponse response)
        {

            var title = UITitle.Create();

            title.Title = "我的应用";

            var ds = DataFactory.Instance().Site();

            var Keyword = request.SendValues?["Keyword"];
            if (String.IsNullOrEmpty(Keyword) == false)
            {

                ds = ds.Where(r => r.Caption.Contains(Keyword) || r.Root.Contains(Keyword) || r.Domain.Contains(Keyword)).Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
            }
            else
            {

                ds = ds.Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
            }

            var ui = UISection.Create(title);
            var webr = UMC.Data.WebResource.Instance();

            foreach (var d in ds)
            {
                var cell = new UIImageTextValue(new Uri(request.Url, webr.ImageResolve(Data.Utility.Guid(d.Root, true).Value, "1", 4)).AbsoluteUri, d.Caption, d.Root);
                cell.Click(new UIClick(request.Model, "Site", d.Root));

                cell.Style.Name("image-width", 72);
                cell.Style.Name("image-radius", 10);
                ui.Add(cell);
            }
            if (ds.Length == 0)
            {
                if (String.IsNullOrEmpty(Keyword))
                {
                    var desc = new UIDesc("未有的应用，请发布新应用");
                    desc.Put("icon", "\uf016").Format("desc", "{icon}\n{desc}");
                    desc.Style.Align(1).Color(0xaaa).Padding(20, 20).BgColor(0xfff).Size(12).Name("icon", new UIStyle().Font("wdk").Size(60));
                    ui.Add(desc);
                }
                else
                {
                    var desc = new UIDesc($"未搜索到“{Keyword}”相关发布的应用");
                    desc.Put("icon", "\uf016").Format("desc", "{icon}\n{desc}");
                    desc.Style.Align(1).Color(0xaaa).Padding(20, 20).BgColor(0xfff).Size(12).Name("icon", new UIStyle().Font("wdk").Size(60));
                    ui.Add(desc);
                }
            }

            ui.SendTo(this.Context, true, $"{request.Model}.{request.Command}");

        }

    }
}