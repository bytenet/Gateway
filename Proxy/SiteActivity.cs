﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Reflection;
using UMC.Web;
using UMC.Data.Entities;
using UMC.Web.UI;
using UMC.Proxy.Entities;
using UMC.Data;
using UMC.Net;

namespace UMC.Proxy.Activities
{
    /// <summary>
    /// 应用管理
    /// </summary>
    [UMC.Web.Mapping("Proxy", "Site", Auth = WebAuthType.User)]
    public class SiteActivity : WebActivity
    {

        static void Feilds(Hashtable hashtable, String key)
        {
            var feilds = hashtable["Feilds"] as Hashtable;
            if (feilds != null)
            {

                var fd = feilds.Keys.Cast<String>().OrderBy(r => r).GetEnumerator();

                while (fd.MoveNext())
                {
                    var pconfig = UMC.Data.DataFactory.Instance().Config(key + "_" + fd.Current.ToUpper());
                    if (pconfig != null)
                    {
                        var feld = JSON.Deserialize<Hashtable>(pconfig.ConfValue);
                        feld["Caption"] = feilds[fd.Current];
                        feilds[fd.Current] = feld;
                    }
                    else
                    {
                        var feld = new Hashtable();
                        feld["Caption"] = feilds[fd.Current];
                        feilds[fd.Current] = feld;
                    }
                }
            }
        }
        void Market(Site site)
        {

            var secret = UMC.Data.WebResource.Instance().Provider["appSecret"];
            if (String.IsNullOrEmpty(secret))
            {
                this.Prompt("当前版本未登记注册", false);
                this.Context.Response.Redirect("System", "License");
            }
            if (((site.Flag ?? 0) & 2) == 2)
            {
                this.Prompt("提示", "此是从发布市场安装的应用,不能再提到应用市场");
            }
            if (site.IsMarket ?? false == false)
            {
                var data = PutMarket(site, secret);
                if (String.Equals(data?["code"], "success"))
                {
                    DataFactory.Instance().Put(new Site { IsMarket = true, Root = site.Root });
                }
                else
                {
                    this.Prompt(data?["msg"]);
                }

            }
            this.Context.Response.Redirect("Proxy", "Market", site.Root);
        }
        public static WebMeta PutMarket(Site site, string secret)
        {
            var confData = new Hashtable();
            Reflection.PropertyToDictionary(site, confData);
            confData.Remove("AdminConf");
            confData.Remove("AppendJSConf");
            confData.Remove("AppSecret");
            confData.Remove("AppSecretAuth");
            confData.Remove("ImagesConf");
            confData.Remove("IsModule");
            confData.Remove("IsAuth");
            confData.Remove("IsDebug");
            confData.Remove("IsMarket");
            confData.Remove("ModifyTime");
            confData.Remove("IsDesktop");
            confData.Remove("SiteKey");
            confData.Remove("HostModel");
            confData.Remove("Conf");

            var login = UMC.Data.DataFactory.Instance().Config($"SITE_MIME_{site.Root}_LOGIN".ToUpper());
            if (login != null)
            {
                var hash = JSON.Deserialize<Hashtable>(login.ConfValue);
                Feilds(hash, login.ConfKey);
                confData["login"] = hash;
            }

            login = UMC.Data.DataFactory.Instance().Config($"SITE_MIME_{site.Root}_CHECK".ToUpper());
            if (login != null)
            {
                var hash = JSON.Deserialize<Hashtable>(login.ConfValue);
                Feilds(hash, login.ConfKey);
                confData["check"] = hash;
            }
            login = UMC.Data.DataFactory.Instance().Config($"SITE_MIME_{site.Root}_UPDATE".ToUpper());
            if (login != null)
            {
                var hash = JSON.Deserialize<Hashtable>(login.ConfValue);
                Feilds(hash, login.ConfKey);
                confData["update"] = hash;
            }
            login = UMC.Data.DataFactory.Instance().Config($"SITE_MIME_{site.Root}_LOGIN_HTML".ToUpper());
            if (login != null)
            {
                confData["loginHtml"] = login.ConfValue;
            }


            var appendJSConf = SiteConfig.Config(site.AppendJSConf);
            var appendConf = new Hashtable();
            foreach (var append in appendJSConf)
            {
                Config pconfig = new Config();
                var md5Key = SiteActivity.MD5(append);

                var config = UMC.Data.DataFactory.Instance().Config(String.Format("SITE_JS_CONFIG_{0}{1}", site.Root, md5Key).ToUpper());
                if (config != null)
                {
                    appendConf[append] = config.ConfValue;
                }
            }
            if (appendConf.Count > 0)
            {
                confData["appendJsConf"] = appendConf;
            }


            appendJSConf = SiteConfig.Config(site.ImagesConf);
            var confImage = new Hashtable();
            foreach (var append in appendJSConf)
            {
                Config pconfig = new Config();
                var md5Key = SiteActivity.MD5(append);

                var config = UMC.Data.DataFactory.Instance().Config($"SITE_IMAGE_CONFIG_{site.Root}{md5Key}".ToUpper());
                if (config != null)
                {
                    confImage[append] = config.ConfValue;
                }
            }
            if (confImage.Count > 0)
            {
                confData["confImage"] = confImage;
            }
            var data = JSON.Deserialize<WebMeta>(UMC.Proxy.Utility.Sign(new Uri(APIProxy.Uri, "Transfer").WebRequest(), secret).Put(new WebMeta().Put("type", "AppGoods").Put("value", confData)).ReadAsString());
            return data;

        }


        public void OutputCookie(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "过期会话" };
                from.AddTextarea("Cookie名称", "OutputCookie", site.OutputCookies).PlaceHolder("Cookie的名称和路径").NotRequired();
                from.AddFooter("当从新登录时需要清空的Cookie,值为name和path字典对。");
                from.Submit("确认", "Site.Config");
                return from;
            });
            String OutputCookie = config["OutputCookie"] ?? String.Empty;

            DataFactory.Instance().Put(new Site { OutputCookies = OutputCookie, Root = site.Root });
            this.Context.Send("Site.Config", true);
        }
        public void Delete(Site site)
        {

            this.AsyncDialog("Config", g =>
             {
                 return new Web.UIConfirmDialog("您确认移除此应用吗") { Title = "移除提示" };
             });
            if (site.IsModule != true)
            {
                this.Prompt("请先把应用设置为隐藏应用，再来删除");
            }

            DataFactory.Instance().Put(new Site { Flag = -1, Root = site.Root });

            this.Context.Send("Site.Config", true);
        }

        public void Path(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "新增应用目录" };
                from.AddText("目录", "Path", String.Empty).PlaceHolder("以/开始的路径");
                from.AddText("应用", "Value", String.Empty);
                from.Submit("确认", "Site.Config");
                from.AddFooter("当目录最后为字符为“*”时,表示取后面的路径为子应用的请求路径");
                return from;
            });
            var Key = config["Path"];
            if (Key.StartsWith("/") == false)
            {
                this.Prompt("虚拟目录需要以/开头");
            }
            var Value = config["Value"];
            var site2 = DataFactory.Instance().Site(Value);
            if (site2 == null)
            {
                this.Prompt("未找到此标志的应用");
            }
            var path = new Hashtable();

            if (String.IsNullOrEmpty(site.SubSite) == false)
            {
                var v = UMC.Data.JSON.Deserialize(site.SubSite) as Hashtable;
                if (v != null)
                {
                    path = v;
                }
            }
            path[Key] = Value;
            site.SubSite = UMC.Data.JSON.Serialize(path);
            DataFactory.Instance().Put(new Site
            {
                Root = site.Root,
                SubSite = site.SubSite
            });

            this.Context.Send("Site.Config", true);
        }
        void TimeOut(Site site)
        {
            var config = this.AsyncDialog("TimeOut", g =>
            {
                var from = new Web.UIFormDialog() { Title = "应用时效" };
                from.AddNumber("请求超时", "Timeout", site.Timeout).PlaceHolder("单位为秒，默认10秒");
                from.AddNumber("登录过期", "AuthExpire", site.AuthExpire).PlaceHolder("单位为分,默认30分钟");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Timeout = UMC.Data.Utility.IntParse(config["Timeout"], 100);
            var AuthExpire = UMC.Data.Utility.IntParse(config["AuthExpire"], 0);
            if (Timeout <= 0 || AuthExpire < 0)
            {
                this.Prompt("时间不能小于零");
            }

            DataFactory.Instance().Put(new Site { Root = site.Root, AuthExpire = AuthExpire, Timeout = Timeout });
            this.Context.Send("Site.Config", true);
        }
        void HeaderConf(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "请求配置" };
                from.AddTextarea("请求头配置", "HeaderConf", site.HeaderConf).PlaceHolder("字典配置格式").Put("Rows", 8).NotRequired();
                from.AddFooter("将会追加请求的Header上，当值为HOST、SCHEME、ADDRESS将会分别替换成当前值");

                from.Submit("确认", "Site.Config");
                return from;
            });
            var HostReConf = config["HeaderConf"] ?? String.Empty;


            DataFactory.Instance().Put(new Site { Root = site.Root, HeaderConf = HostReConf });
            this.Context.Send("Site.Config", true);
        }
        void HostReConf(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "内容转化配置" };
                from.AddTextarea("转化路径", "HostReConf", site.HostReConf).PlaceHolder("字典配置格式").Put("Rows", 8).NotRequired();
                from.AddFooter("值可为rp、rm、in其中rp表示替换域名、rm表示移除域名、in表示提交内容域名转化");

                from.Submit("确认", "Site.Config");
                return from;
            });
            var HostReConf = config["HostReConf"];


            DataFactory.Instance().Put(new Site { Root = site.Root, HostReConf = HostReConf });
            this.Context.Send("Site.Config", true);
        }
        void LogoutPath(Site site)
        {
            var sValue = this.AsyncDialog("Settings", g =>
            {
                var from2 = new UIFormDialog() { Title = "触发登录页面" };
                from2.AddTextarea("触发登录页面", "LogoutPath", site.LogoutPath as string).Put("Rows", 5).NotRequired();

                from2.AddFooter("结尾是“$”则表示从后比对，多项用换行、空格或逗号符分割");
                from2.Submit("确认", "Site.Config");
                return from2;
            })["LogoutPath"] ?? String.Empty;

            DataFactory.Instance().Put(new Site
            {
                LogoutPath = sValue,
                Root = site.Root
            });
            this.Context.Send("Site.Config", true);
        }
        void Home(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "应用设置" };
                from.AddText("应用名称", "Caption", site.Caption);
                from.AddText("缓存版本", "Version", site.Version).NotRequired();

                from.AddCheckBox("云桌面", "Setings", "0")
                .Put("桌面展示", "IsDesktop", site.IsDesktop == true)
                .Put("隐藏应用", "IsModule", site.IsModule == true);


                from.Submit("确认", "Site.Config");
                return from;
            });


            var version = config["Version"];
            if (String.IsNullOrEmpty(version) == false)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(version, "^[\\.0-9]+$") == false)
                {
                    this.Prompt("版本号只支持数字和点");
                }
            }
            else
            {
                version = null;
            }
            var confgiSite = new Site
            {
                Caption = config["Caption"],
                Version = version,
                IsDesktop = false,
                IsModule = false,
                Root = site.Root
            };

            var sDoHtmlType = config["Setings"].Split(',');
            foreach (var v in sDoHtmlType)
            {
                switch (v)
                {
                    case "IsDesktop":
                        confgiSite.IsDesktop = true;
                        break;
                    case "IsModule":
                        confgiSite.IsModule = true;
                        break;
                }
            }

            DataFactory.Instance().Put(confgiSite);
            this.Context.Send("Site.Config", true);
        }
        void AppSecret(Site site)
        {
            var config = UIDialog.AsyncDialog(this.Context, "AppSecret", g =>
            {
                var AppSecretAuth = site.AppSecretAuth ?? 0;
                var from = new Web.UIFormDialog() { Title = "应用安全码" };
                from.AddCheckBox("访问权限", "AppSecret", "0").Put("单点登录", "1", (AppSecretAuth & 1) > 0).Put("创建令牌", "2", (AppSecretAuth & 2) > 0);
                from.Submit("确认修改", "Site.Config");
                from.Add(UICell.UI("显示安全码", "", new UIClick(new WebMeta(this.Context.Request.Arguments).Put(g, "View")).Post(this.Context.Request)));
                from.Add(UICell.UI("重置安全码", "", new UIClick(new WebMeta(this.Context.Request.Arguments).Put(g, "Reset")).Post(this.Context.Request)));
                from.Add(UICell.UI("随机重置", "", new UIClick(new WebMeta(this.Context.Request.Arguments).Put(g, "Round")).Post(this.Context.Request)));
                return from;
            });
            switch (config)
            {

                case "Round":

                    this.AsyncDialog("Confim", g => new Web.UIConfirmDialog("随机重置后，历史安全码将不可用，确认吗"));
                    site.AppSecret = Utility.Guid(Guid.NewGuid());
                    DataFactory.Instance().Put(new Site { Root = site.Root, AppSecret = site.AppSecret });
                    this.Prompt("应用安全码", "AppSecret：" + site.AppSecret);
                    break;
                case "Reset":
                    var Value = UIDialog.AsyncDialog(this.Context, "Value", g =>
                    {
                        var from = new Web.UIFormDialog() { Title = "重置安全码" };
                        from.AddText("安全码", "Value", string.Empty);
                        from.AddFooter("重置后，当前安全码将不可用");
                        return from;
                    });
                    DataFactory.Instance().Put(new Site { Root = site.Root, AppSecret = Value });
                    break;
                case "View":
                    this.Prompt("应用安全码", "AppSecret：" + site.AppSecret);
                    break;
                default:
                    var AppSecret = 0;
                    foreach (var v in config.Split(','))
                    {
                        AppSecret |= Utility.IntParse(v, 0);
                    }
                    DataFactory.Instance().Put(new Site { Root = site.Root, AppSecretAuth = AppSecret });
                    break;
            }

        }

        void Setting(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "应用设置" };

                from.AddText("应用主页", "Home", site.Home).NotRequired();
                from.AddText("移动主页", "MobileHome", site.MobileHome).NotRequired();



                from.AddRadio("访问许可", "AuthType")
                .Put("所有人", "All", site.AuthType == WebAuthType.All)
                .Put("匿名检查", "Check", site.AuthType == WebAuthType.Check)
                .Put("登录人员", "Guest", site.AuthType == WebAuthType.Guest)
                .Put("内部用户", "User", site.AuthType == WebAuthType.User)
                .Put("用户检查", "UserCheck", site.AuthType == WebAuthType.UserCheck)
                .Put("管理员", "Admin", site.AuthType == WebAuthType.Admin);


                from.AddCheckBox("", "Setings", "0")
                .Put("开启日志", "IsDebug", site.IsDebug == true)
                .Put("强化验证", "IsAuth", site.IsAuth == true);

                from.Submit("确认", "Site.Config");


                var userBrowser = site.UserBrowser ?? Entities.UserBrowser.All;
                from.AddCheckBox("支持浏览器", "UserBrowser", "All")
                .Put("IE", "IE", (userBrowser & UserBrowser.IE) == UserBrowser.IE)
                .Put("谷歌", "Chrome", (userBrowser & UserBrowser.Chrome) == UserBrowser.Chrome)
                .Put("火狐", "Firefox", (userBrowser & UserBrowser.Firefox) == UserBrowser.Firefox)
                .Put("钉钉", "Dingtalk", (userBrowser & UserBrowser.Dingtalk) == UserBrowser.Dingtalk)
                .Put("微信", "WeiXin", (userBrowser & UserBrowser.WeiXin) == UserBrowser.WeiXin)
                .Put("WebKit", "WebKit", (userBrowser & UserBrowser.WebKit) == UserBrowser.WebKit);




                from.AddRadio("云桌面打开方式", "OpenModel")
                .Put("新窗口", "0", (site.OpenModel ?? 0) == 0)
                .Put("当前窗口", "1", site.OpenModel == 1)
                .Put("最大化窗口", "2", site.OpenModel == 2)
                .Put("快捷方式", "3", site.OpenModel == 3);
                return from;
            });
            var Home = config["Home"];
            if (String.IsNullOrEmpty(Home) == false)
            {
                if (Home.StartsWith("https://") == false && Home.StartsWith("http://") == false)
                {
                    if (Home.StartsWith("/") == false || Home.StartsWith("//"))
                    {
                        this.Prompt("主页格式不正确，请确认");
                    }
                }
            }
            else
            {
                Home = null;
            }
            var MobileHome = config["MobileHome"];
            if (String.IsNullOrEmpty(MobileHome) == false)
            {
                if (MobileHome.StartsWith("https://") == false && MobileHome.StartsWith("http://") == false)
                {
                    if (MobileHome.StartsWith("/") == false || MobileHome.StartsWith("//"))
                    {
                        this.Prompt("主页格式不正确，请确认");
                    }
                }
            }
            else
            {
                MobileHome = null;
            }
            var confgiSite = new Site
            {
                Home = Home,
                MobileHome = MobileHome,
                OpenModel = UMC.Data.Utility.Parse(config["OpenModel"], 0),
                IsDebug = false,
                IsAuth = false,
                AuthType = UMC.Data.Utility.Parse(config["AuthType"], UMC.Web.WebAuthType.User),
                Root = site.Root
            };

            var userBrowser2 = UserBrowser.All;
            foreach (var v in config["UserBrowser"].Split(','))
            {
                userBrowser2 |= UMC.Data.Utility.Parse(v, UserBrowser.All);
            }
            confgiSite.UserBrowser = userBrowser2;

            var sDoHtmlType = config["Setings"].Split(',');
            foreach (var v in sDoHtmlType)
            {
                switch (v)
                {
                    case "IsDebug":
                        confgiSite.IsDebug = true;
                        break;
                    case "IsAuth":
                        confgiSite.IsAuth = true;
                        break;
                }
            }
            DataFactory.Instance().Put(confgiSite);
            this.Context.Send("Site.Config", true);
        }
        public static string MD5(String src)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            byte[] md = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(src));
            return UMC.Data.Utility.Parse36Encode(UMC.Data.Utility.IntParse(md));
        }
        public void Share(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "设置共享模式" };
                var m = "标准模式";
                switch ((site.UserModel ?? UserModel.Standard))
                {
                    case UserModel.Bridge:
                        m = "桥接模式";
                        break;
                    case UserModel.Share:
                        m = "共享模式";
                        break;
                    case UserModel.Quote:
                        m = "引用模式";
                        break;
                    case UserModel.Check:
                        m = "自主检测";
                        break;
                    case UserModel.Checked:
                        m = "自动检测";
                        break;
                }
                from.AddTextValue().Put("当前模式", m);

                from.AddText("共享账户", "Account", site.Account).PlaceHolder("多账户可用|和~分割");

                from.AddText("共同密码", "Password", String.Empty);
                from.Submit("确认", "Site.Config");
                return from;
            });
            var acount = config["Account"];
            var pwd = config["Password"];

            var site2 = new Site()
            {
                Root = site.Root,
                Account = acount,
                UserModel = UserModel.Share
            };
            if (String.IsNullOrEmpty(pwd) == false)
            {
                UMC.Data.DataFactory.Instance().Password(SiteConfig.MD5Key(site.Root, acount), pwd);
            }
            DataFactory.Instance().Put(site2);
            this.Context.Send("Site.Config", true);
        }
        public void SetHostModel(Site site)
        {
            var config = this.AsyncDialog("HostModel", g =>
            {
                var from = new Web.UISheetDialog() { Title = "认证切换模式" };
                from.Put("不切换", "None").Put("登录页切换", "Login").Put("浏览器中切换", "Check")
                .Put("全域名切换", "Disable");

                return from;
            });
            var acount = UMC.Data.Utility.Parse(config, HostModel.None);
            DataFactory.Instance().Put(new Site
            {
                HostModel = acount,
                Root = site.Root
            });
            this.Context.Send("Site.Config", true);
        }
        public void UseModel(Site site)
        {
            var config = this.AsyncDialog("UserModel", g =>
            {
                var from = new Web.UISheetDialog() { Title = "设置账户模式" };

                from.Put("标准模式", "Standard").Put("桥接模式", "Bridge");
                var arg = this.Context.Request.Arguments;
                var m = this.Context.Request.Model;
                var c = this.Context.Request.Command;
                from.Put(new UIClick(m, c, new WebMeta(arg).Put("Model", "Quote")) { Text = "引用模式" });
                from.Put(new UIClick(m, c, new WebMeta(arg).Put("Model", "Share")) { Text = "共享模式" });


                return from;
            });
            var acount = UMC.Data.Utility.Parse(config, UserModel.Standard);
            DataFactory.Instance().Put(new Site
            {
                UserModel = acount,
                Root = site.Root
            });
            this.Context.Send("Site.Config", true);
        }
        public void Quote(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "设置引用模式" };
                var ak = "";
                if (String.IsNullOrEmpty(site.Account) == false && site.Account.StartsWith("@"))
                {
                    ak = site.Account.Substring(1);
                }
                var m = "标准模式";
                switch ((site.UserModel ?? UserModel.Standard))
                {
                    case UserModel.Bridge:
                        m = "桥接模式";
                        break;
                    case UserModel.Share:
                        m = "共享模式";
                        break;
                    case UserModel.Quote:
                        m = "引用模式";
                        break;
                    case UserModel.Check:
                        m = "自主检测";
                        break;
                    case UserModel.Checked:
                        m = "自动检测";
                        break;
                }
                from.AddTextValue().Put("当前模式", m);

                from.AddText("引用应用", "Account", ak);
                from.Submit("确认", "Site.Config");
                return from;
            });
            var acount = config["Account"];

            var site2 = DataFactory.Instance().Site(acount);
            if (site2 == null)
            {
                this.Prompt("未有引用的应用");
            }
            if (site2.Root == site.Root)
            {
                this.Prompt("引用应用不能配置自己");
            }
            switch (site2.UserModel ?? UserModel.Standard)
            {
                case UserModel.Standard:
                case UserModel.Share:
                case UserModel.Checked:
                case UserModel.Check:
                    break;
                default:
                    this.Prompt("应用不支持引用对接");
                    break;
            }
            DataFactory.Instance().Put(new Site
            {
                Account = "@" + acount,
                UserModel = UserModel.Quote,
                Root = site.Root
            });
            this.Context.Send("Site.Config", true);
        }
        public void Account(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "检测账户" };
                from.AddText("账户名称", "Account", site.Account);
                from.AddText("账户密码", "Password", String.Empty).NotRequired();
                from.Submit("确认", "Mime.Config");
                return from;
            });
            var acount = config["Account"];
            var pwd = config["Password"];

            var site2 = new Site()
            {
                Root = site.Root,
                Account = acount
            };
            if (String.IsNullOrEmpty(pwd) == false)
            {
                UMC.Data.DataFactory.Instance().Password(SiteConfig.MD5Key(site.Root, acount), pwd);
            }
            DataFactory.Instance().Put(site2);

            this.Context.Send("Mime.Config", true);
        }
        void Copy(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "复制应用" };

                from.AddText("新标识", "Root", String.Empty).PlaceHolder("[a-z0-9]全小写字符");

                from.AddText("新名称", "Caption", site.Caption);
                from.Submit("确认", "Site.Config");
                return from;
            });
            var newRoot = config["Root"].ToLower();
            if (System.Text.RegularExpressions.Regex.IsMatch(newRoot, "^[a-z0-9]+$") == false)
            {
                this.Prompt("应用标识只支持【a-z0-9】字符");
            }



            var oldSite = DataFactory.Instance().Site(newRoot);
            if (oldSite != null)
            {
                this.Prompt("此应用标识已注册");
            }


            CopyMime("SITE_MIME_{0}_LOGIN", site.Root, newRoot);
            CopyMime("SITE_MIME_{0}_CHECK", site.Root, newRoot);
            CopyMime("SITE_MIME_{0}_UPDATE", site.Root, newRoot);


            var jsPaths = SiteConfig.Config(site.AppendJSConf);
            foreach (var key in jsPaths)
            {
                var md5Key = MD5(key as string);
                var jsKey = String.Format("SITE_JS_CONFIG_{0}{1}", site.Root, md5Key).ToUpper();

                var pconfig = UMC.Data.DataFactory.Instance().Config(jsKey);
                if (pconfig != null)
                {
                    pconfig.ConfKey = String.Format("SITE_JS_CONFIG_{0}{1}", newRoot, md5Key).ToUpper();
                    UMC.Data.DataFactory.Instance().Put(pconfig);
                }
            }
            var Images = SiteConfig.Config(site.ImagesConf);
            foreach (var key in Images)
            {
                var md5Key = MD5(key as string);
                var jsKey = String.Format("SITE_IMAGE_CONFIG_{0}{1}", site.Root, md5Key).ToUpper();

                var pconfig = UMC.Data.DataFactory.Instance().Config(jsKey);
                if (pconfig != null)
                {
                    pconfig.ConfKey = String.Format("SITE_IMAGE_CONFIG_{0}{1}", newRoot, md5Key).ToUpper();
                    UMC.Data.DataFactory.Instance().Put(pconfig);
                }
            }


            var htmlconfig = UMC.Data.DataFactory.Instance().Config($"SITE_MIME_{site.Root}_LOGIN_HTML".ToUpper());
            if (htmlconfig != null)
            {
                htmlconfig.ConfKey = $"SITE_MIME_{newRoot}_LOGIN_HTML".ToUpper();
                UMC.Data.DataFactory.Instance().Put(htmlconfig);
            }

            site.Root = newRoot;
            site.Caption = config["Caption"];
            site.SiteKey = UMC.Data.Utility.IntParse(UMC.Data.Utility.Guid(newRoot, true).Value);
            DataFactory.Instance().Put(site);

            String webrkey = String.Format("images/{0}/{1}/{2}.png", site.Root, 1, 0);
            using (System.IO.Stream stream = typeof(WebServlet).Assembly
                                    .GetManifestResourceStream("UMC.Proxy.Resources.app.png"))
            {
                UMC.Data.WebResource.Instance().Transfer(stream, webrkey);
            }

            this.Prompt("应用复制成功", false);
            this.Context.Send("Site.Config", true);
            this.Context.Response.Redirect(this.Context.Request.Model, this.Context.Request.Command, site.Root);
        }
        void CopyMime(String formt, String old, String root)
        {

            var oldKey = String.Format(formt, old).ToUpper();
            var newKey = String.Format(formt, root).ToUpper();

            var pconfig = UMC.Data.DataFactory.Instance().Config(oldKey);
            if (pconfig == null)
            {
                return;

            }
            var config = new Hashtable();
            var v = UMC.Data.JSON.Deserialize(pconfig.ConfValue) as Hashtable;
            if (v != null)
            {
                config = v;
            }
            pconfig.ConfKey = newKey;
            UMC.Data.DataFactory.Instance().Put(pconfig);

            var feilds = config["Feilds"] as Hashtable;
            if (feilds != null && feilds.Count > 0)
            {
                var fd = feilds.Keys.Cast<String>().OrderBy(r => r).GetEnumerator();

                while (fd.MoveNext())
                {

                    var fconfig = UMC.Data.DataFactory.Instance().Config($"{oldKey}_{fd.Current}".ToUpper());
                    if (fconfig != null)
                    {
                        fconfig.ConfKey = $"{newKey}_{fd.Current}".ToUpper();
                        UMC.Data.DataFactory.Instance().Put(fconfig);
                    }

                }

            }

        }

        public override void ProcessActivity(WebRequest request, WebResponse response)
        {

            var Key = this.AsyncDialog("Key", g =>
            {
                var form = request.SendValues ?? new WebMeta();
                var limit = form["limit"] ?? "none";
                switch (limit)
                {
                    case "PC":
                        {
                            var sts = new System.Data.DataTable();
                            sts.Columns.Add("id");
                            sts.Columns.Add("name");
                            sts.Columns.Add("root");
                            sts.Columns.Add("domain");
                            sts.Columns.Add("module");
                            sts.Columns.Add("auth");
                            var ds = DataFactory.Instance().Site();

                            var Keyword = form["Keyword"];
                            if (String.IsNullOrEmpty(Keyword) == false)
                            {

                                ds = ds.Where(r => r.Caption.Contains(Keyword) || r.Root.Contains(Keyword) || r.Domain.Contains(Keyword)).Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
                            }
                            else
                            {

                                ds = ds.Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
                            }
                            foreach (var d in ds)
                            {
                                var dtype = "桌面展示";
                                if (d.IsModule == true)
                                {
                                    dtype = "应用隐藏";
                                }
                                else if (d.IsDesktop ?? false == false)
                                {
                                    dtype = "桌面展示";
                                }
                                var Domain = d.Domain ?? "";

                                var domains = Domain.Split(new char[] { ',', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                                sts.Rows.Add(d.SiteKey ?? UMC.Data.Utility.IntParse(UMC.Data.Utility.Guid(d.Root, true).Value), d.Caption, d.Root, domains.Length > 1 ? "多例均衡" : (domains[0].StartsWith("file:") ? "文件系统" : domains[0]), dtype, d.AuthType ?? Web.WebAuthType.All);

                            }

                            var rdata = new WebMeta().Put("data", sts);
                            response.Redirect(request.IsMaster ? rdata.Put("IsMaster", true) : rdata);
                        }
                        break;
                    default:
                        {
                            var title = UITitle.Create();

                            title.Title = "我的应用";

                            var ds = DataFactory.Instance().Site();

                            var Keyword = form["Keyword"];
                            if (String.IsNullOrEmpty(Keyword) == false)
                            {

                                ds = ds.Where(r => r.Caption.Contains(Keyword) || r.Root.Contains(Keyword) || r.Domain.Contains(Keyword)).Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
                            }
                            else
                            {

                                ds = ds.Where(r => r.Flag != -1).OrderBy(r => r.Caption).ToArray();
                            }

                            var ui = UISection.Create(new UIHeader().Search("搜索"), title);
                            var webr = UMC.Data.WebResource.Instance();

                            foreach (var d in ds)
                            {
                                var cell = new UIImageTextValue(webr.ImageResolve(d.Root, "1", 4), d.Caption, d.Root);
                                cell.Click(new UIClick(new WebMeta().Put(g, d.Root)).Post(request.Model, request.Command));

                                cell.Style.Name("image-width", 72);
                                cell.Style.Name("image-radius", 10);
                                ui.Add(cell);
                            }
                            if (ds.Length == 0)
                            {
                                if (String.IsNullOrEmpty(Keyword))
                                {
                                    var desc = new UIDesc("未有的应用，请发布新应用");
                                    desc.Put("icon", "\uf016").Format("desc", "{icon}\n{desc}");
                                    desc.Style.Align(1).Color(0xaaa).Padding(20, 20).BgColor(0xfff).Size(12).Name("icon", new UIStyle().Font("wdk").Size(60));
                                    ui.Add(desc);
                                }
                                else
                                {
                                    var desc = new UIDesc($"未搜索到“{Keyword}”相关发布的应用");
                                    desc.Put("icon", "\uf016").Format("desc", "{icon}\n{desc}");
                                    desc.Style.Align(1).Color(0xaaa).Padding(20, 20).BgColor(0xfff).Size(12).Name("icon", new UIStyle().Font("wdk").Size(60));
                                    ui.Add(desc);
                                }
                            }
                            if (request.IsMaster)
                            {
                                ui.UIFootBar = new UIFootBar() { IsFixed = true };
                                ui.UIFootBar.AddText(new UIEventText("网关服务").Click(new UIClick().Post(request.Model, "Server")),
                                 new UIEventText("发布新应用").Click(new UIClick(new WebMeta(request.Arguments).Put(g, "Create")).Post(request.Model, request.Command)).Style(new UIStyle().BgColor()));
                            }
                            ui.SendTo(this.Context, true, $"{request.Model}.{request.Command}");
                        }
                        break;
                }


                return this.DialogValue("none");
            });
            switch (Key)
            {
                case "Create":
                    this.Context.Send(new UIClick(request.Model, "Setup"), true);
                    return;
            }
            var site = DataFactory.Instance().Site(Key);

            var ms = request.SendValues ?? request.Arguments;
            var Model = this.AsyncDialog("Model", g =>
            {
                if (ms.ContainsKey("limit") == false)
                {
                    if (site == null)
                    {
                        site = DataFactory.Instance().Site(UMC.Data.Utility.IntParse(Key, 0));
                        if (site != null)
                        {
                            request.Arguments.Put("Key", site.Root);
                        }
                    }
                }
                var isFile = SiteConfig.Config(site.Domain).Any(r => r.StartsWith("file://"));
                // var IsUMC = false;
                var AuthConf = SiteConfig.Config(site.AuthConf);
                var IsProxy = AuthConf.Contains("*");
                var IsShow = ms["Show"] == "true";
                if (IsProxy == false)
                {
                    IsShow = true;
                }
                if (isFile)
                {
                    IsShow = false;
                }
                var title = UITitle.Create();

                title.Title = "应用配置";


                var ui = UISection.Create(title);
                // var imageId = Data.Utility.Guid(site.Root, true);

                var imageTextView = new UMC.Web.UI.UIImageTextValue(new Uri(request.Url, Data.WebResource.Instance().ImageResolve(site.Root, "1", 4, $"_t={site.ModifyTime}")).AbsoluteUri, "", "图标");
                imageTextView.Style.Name("image-width", "100");
                imageTextView.Click(new UIClick(request, g, "Image"));

                ui.Add(imageTextView);

                ui.AddCell("应用标识", site.Root, IsShow ? new UIClick(new WebMeta(request.Arguments).Put(g, "Setting")).Post(request.Model, request.Command) : null)

                 .AddCell("应用名称", site.Caption, new UIClick(new WebMeta(request.Arguments).Put(g, "Home")).Post(request.Model, request.Command));


                var domains = site.Domain.Split(new char[] { ',', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                if (isFile)
                {
                    ui.NewSection().AddCell("负载网址", domains.Length > 1 ? "多例均衡" : (domains[0].StartsWith("file:") ? "文件系统" : domains[0]), new UIClick(new WebMeta(request.Arguments).Put(g, "Domain")).Post(request.Model, request.Command));
                }
                else
                {
                    ui.NewSection().AddCell("负载网址", domains.Length > 1 ? "多例均衡" : (domains[0].StartsWith("file:") ? "文件系统" : domains[0]), new UIClick(new WebMeta(request.Arguments).Put(g, "Domain")).Post(request.Model, request.Command))

                    .AddCell("请求配置", String.IsNullOrEmpty(site.HeaderConf) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "HeaderConf")).Post(request.Model, request.Command));
                    ui.NewSection().AddCell("动静分离", String.IsNullOrEmpty(site.StaticConf) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "StaticConf")).Post(request.Model, request.Command))
                              .AddCell("日志参数", String.IsNullOrEmpty(site.LogConf) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "LogConf")).Post(request.Model, request.Command));
                    if (IsShow)
                    {
                        ui.NewSection().AddCell("桌面授权", "", new UIClick(new WebMeta().Put("Key", site.Root, "Model", "Auth")).Post(this.Context.Request.Model, this.Context.Request.Command))
                       .AddCell("应用安全码", new UIClick(new WebMeta(request.Arguments).Put(g, "AppSecret")).Post(request.Model, request.Command));
                    }
                }
                var imageUI = ui.NewSection().AddCell("图片处理", "配置", new UIClick(new WebMeta(request.Arguments).Put(g, "ImagesConf")).Post(request.Model, request.Command));


                var imagePaths = SiteConfig.Config(site.ImagesConf);
                foreach (var key in imagePaths)
                {
                    imageUI.AddCell(key, new UIClick(request.Model, "ConfImage", String.Format("SITE_IMAGE_CONFIG_{0}{1}", site.Root, MD5(key as string)).ToUpper()));
                }





                var ui2 = ui.NewSection().AddCell("应用目录", "配置", new UIClick(new WebMeta(request.Arguments).Put(g, "Path")).Post(request.Model, request.Command));


                var path = new Hashtable();

                if (String.IsNullOrEmpty(site.SubSite) == false)
                {
                    var v = UMC.Data.JSON.Deserialize(site.SubSite) as Hashtable;
                    if (v != null)
                    {
                        path = v;
                    }
                }
                var pem = path.GetEnumerator();
                while (pem.MoveNext())
                {
                    var pcell = UICell.Create("UI", new WebMeta().Put("value", pem.Value.ToString(), "text", pem.Key.ToString()));
                    ui2.Delete(pcell, new UIEventText("移除").Click(new UIClick(new WebMeta(request.Arguments).Put(g, pem.Key.ToString())).Post(request.Model, request.Command)));
                }

                var hosts = DataFactory.Instance().Hosts(site.Root);
                ui2 = ui.NewSection().AddCell("应用域名", "配置", new UIClick(new WebMeta(request.Arguments).Put(g, "Host")).Post(request.Model, request.Command));


                var home = UMC.Data.WebResource.Instance().WebDomain();
                if (home == "localhost")
                {
                    var v = UICell.UI('\uf015', $"{site.Root}.[主域名]", "主域名未配置", new UIClick().Post(request.Model, "Server"));
                    v.Style.Name("value").Color(0xc00);
                    ui2.Add(v);
                }
                else
                {
                    ui2.AddCell('\uf015', $"[{site.Root}.{home}]", "", new UIClick("Model", "CSR", "Domain", $"{site.Root}.{home}").Post(request.Model, "Server"));


                }
                var isHostProxy = false;
                foreach (var h in hosts)
                {
                    var Scheme = String.Empty;
                    switch (h.Scheme ?? 0)
                    {
                        case 2:
                            Scheme = "Https";
                            break;
                    }
                    var pcell = UICell.UI(h.IsAuthModel == true ? '\uf13e' : '\uf0c1', h.Host, Scheme, new UIClick("Model", "CSR", "Domain", h.Host).Post(request.Model, "Server"));
                    ui2.Delete(pcell, new UIEventText("移除").Click(new UIClick(new WebMeta(request.Arguments).Put(g, h.Host)).Post(request.Model, request.Command)));
                    if (h.IsAuthModel == true)
                    {
                    }
                    else
                    {
                        isHostProxy = true;
                    }

                }

                if (isHostProxy && IsShow)
                {
                    var hm = "不切换";
                    switch ((site.HostModel ?? HostModel.None))
                    {
                        case HostModel.Login:
                            hm = "登录页切换";
                            break;
                        case HostModel.Check:
                            hm = "浏览器中切换";
                            break;
                        case HostModel.Disable:
                            hm = "全域名切换";
                            break;
                    }

                    ui2 = ui2.NewSection().AddCell("切换认证", hm, new UIClick(new WebMeta(request.Arguments).Put(g, "HostModel")).Post(request.Model, request.Command));
                    ui2.AddCell("切换地址", "配置", new UIClick(new WebMeta(request.Arguments).Put(g, "RedirectPath")).Post(request.Model, request.Command));
                    var redPaths = SiteConfig.Config(site.RedirectPath);
                    foreach (var key in redPaths)
                    {
                        ui2.AddCell(key, new UIClick(request.Model, "Conf", String.Format("SITE_JS_CONFIG_{0}{1}", site.Root, MD5(key as string)).ToUpper()));
                    }
                    ui2.Header.Put("text", "反代域名设置");
                }
                if (IsShow)
                {
                    if (IsProxy == false && UserModel.Bridge == site.UserModel && AuthConf.Contains("#"))
                    {
                        ui.NewSection()
                         .AddCell("许可路径", "轻应用模式", new UIClick(new WebMeta(request.Arguments).Put(g, "AuthConf")).Post(request.Model, request.Command))
                                         .AddCell("云模块加载", "配置", new UIClick(request.Model, "UMC", site.Root));

                    }
                    else
                    {
                        ui.NewSection()
                                .AddCell("许可路径", String.IsNullOrEmpty(site.AuthConf) ? "未设置" : (IsProxy ? "代理转发" : "已设置"), new UIClick(new WebMeta(request.Arguments).Put(g, "AuthConf")).Post(request.Model, request.Command))
                          .AddCell("应用时效", String.Format("{0}s:{1}m", site.Timeout ?? 100, site.AuthExpire ?? 30), new UIClick(new WebMeta(request.Arguments).Put(g, "Timeout")).Post(request.Model, request.Command));

                    }


                    ui2 = ui.NewSection().AddCell("追加脚本", "配置", new UIClick(new WebMeta(request.Arguments).Put(g, "AppendJSConf")).Post(request.Model, request.Command));




                    var jsPaths = SiteConfig.Config(site.AppendJSConf);
                    foreach (var key in jsPaths)
                    {
                        ui2.AddCell(key, new UIClick(request.Model, "Conf", String.Format("SITE_JS_CONFIG_{0}{1}", site.Root, MD5(key as string)).ToUpper()));
                    }

                    ui2 = ui.NewSection().AddCell("内容转化", String.IsNullOrEmpty(site.HostReConf) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "HostReConf")).Post(request.Model, request.Command));


                    ui2.AddCell("过期会话", String.IsNullOrEmpty(site.OutputCookies) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "OutputCookie")).Post(request.Model, request.Command));

                    var m = "标准模式";
                    switch ((site.UserModel ?? UserModel.Standard))
                    {
                        case UserModel.Bridge:
                            m = "桥接模式";
                            break;
                        case UserModel.Share:
                            m = "共享模式";
                            break;
                        case UserModel.Quote:
                            m = "引用模式";
                            break;
                        case UserModel.Check:
                            m = "自主检测";
                            break;
                        case UserModel.Checked:
                            m = "自动检测";
                            break;
                    }

                    ui.NewSection().AddCell("账户对接模式", m, new UIClick(new WebMeta(request.Arguments).Put(g, "UserModel")).Post(request.Model, request.Command))

                   .AddCell("触发登录页面", String.IsNullOrEmpty(site.LogoutPath) ? "未设置" : "已设置", new UIClick(new WebMeta(request.Arguments).Put(g, "LogoutPath")).Post(request.Model, request.Command));


                    ui.NewSection().AddCell("账户登录接口", new UIClick(request.Model, "Mime", String.Format("{0}_Login", site.Root)));

                    ui.NewSection().AddCell("密码托管接口", new UIClick(request.Model, "Mime", String.Format("{0}_Update", site.Root)));

                    ui.NewSection().AddCell("账户检测接口", new UIClick(request.Model, "Mime", String.Format("{0}_Check", site.Root)));

                    ui.NewSection().AddCell("出售此配置", "上架到应用市场", new UIClick(request, g, "Market"));

                    ui.NewSection().AddCell("联系官方", "让天才工程师为你配置", new UIClick("Contact").Post("System", "License"));


                    ui.UIFootBar = new UIFootBar() { IsFixed = true };
                    ui.UIFootBar.AddText(new UIEventText("应用身份设置").Click(new UIClick(request.Model, "App").Send("Key", site.Root, "Model", "Setting")),
                     new UIEventText("重新加载").Click(new UIClick(request, g, "Reload")).Style(new UIStyle().BgColor()));


                }
                else
                {
                    ui.NewSection().AddCell("出售此配置", "上架到应用市场", new UIClick(request, g, "Market"));

                    ui.UIFootBar = new UIFootBar() { IsFixed = true };
                    ui.UIFootBar.AddText(new UIEventText("网关登录配置").Click(UIClick.Query(new WebMeta().Put("Show", "true"))),
                     new UIEventText("重新加载").Click(new UIClick(request, g, "Reload")).Style(new UIStyle().BgColor()));

                }
                ui.SendTo(this.Context, true, "Site.Config", "System.Picture");
                return this.DialogValue("none");
            });

            if (request.IsMaster == false)
            {
                var rols = UMC.Data.DataFactory.Instance().Roles(this.Context.Token.UserId.Value, site.SiteKey.Value);
                if (rols.Contains(UMC.Security.Membership.AdminRole) == false)
                {
                    this.Prompt("配置应用的需要应用管理员权限");
                }

            }
            switch (Model)
            {
                case "Market":
                    Market(site);
                    break;
                case "Image":

                    DataFactory.Instance().Put(new Site
                    {
                        ModifyTime = Utility.TimeSpan(),
                        Root = site.Root
                    });
                    response.Redirect("System", "Picture", new WebMeta().Put("id", site.Root).Put("seq", "1"), true);
                    break;
                case "HostModel":
                    SetHostModel(site);
                    break;
                case "Copy":
                    if (request.IsMaster == false)
                    {
                        this.Prompt("复制应用需要管理员权限");
                    }
                    this.Copy(site);
                    break;
                case "Auth":
                    if (request.IsMaster == false)
                    {
                        this.Prompt("需要管理员权限");
                    }
                    response.Redirect("Settings", "AuthKey", new WebMeta().Put("Key", $"Desktop/{site.Root}"), true);
                    break;
                case "Quote":
                    this.Quote(site);
                    break;
                case "Share":
                    this.Share(site);
                    break;
                case "UserModel":
                    UseModel(site);
                    break;
                case "Home":
                    this.Home(site);
                    break;
                case "Timeout":
                    this.TimeOut(site);
                    break;
                case "ImagesConf":
                    this.ImagesConf(site);
                    break;
                case "AppendJSConf":
                    this.AppendJSConf(site);
                    break;
                case "HostReConf":
                    this.HostReConf(site);
                    break;
                case "LogoutPath":
                    this.LogoutPath(site);
                    break;
                case "Setting":
                    this.Setting(site);

                    break;
                case "AppSecret":
                    this.AppSecret(site);
                    break;
                case "LogConf":
                    this.LogConf(site);
                    break;
                case "Account":
                    this.Account(site);
                    break;
                case "OutputCookie":
                    this.OutputCookie(site);
                    break;
                case "Path":
                    this.Path(site);
                    break;
                case "Host":
                    this.Host(site);
                    break;
                case "Domain":
                    this.Domain(site);
                    break;
                case "Delete":
                    if (request.IsMaster == false)
                    {
                        this.Prompt("移除应用需要管理员权限");
                    }
                    this.Delete(site);
                    break;
                case "Reload":
                    DataFactory.Instance().Delete(new SiteConfig { Root = site.Root });
                    this.Prompt("重新加载已经就位");
                    break;
                case "HeaderConf":
                    this.HeaderConf(site);
                    break;
                case "AuthConf":
                    this.AuthConf(site);
                    break;
                case "RedirectPath":
                    this.RedirectPath(site);
                    break;
                case "StaticConf":
                    this.StaticConf(site);
                    break;
                default:
                    if (Model.StartsWith("/"))
                    {
                        var path = new Hashtable();

                        if (String.IsNullOrEmpty(site.SubSite) == false)
                        {
                            var v = UMC.Data.JSON.Deserialize(site.SubSite) as Hashtable;
                            if (v != null)
                            {
                                path = v;
                            }
                        }
                        path.Remove(Model);

                        DataFactory.Instance().Put(new Site { Root = site.Root, SubSite = UMC.Data.JSON.Serialize(path) });
                    }
                    else
                    {

                        DataFactory.Instance().Delete(new SiteHost { Host = Model });
                    }

                    break;
            }



        }

        private void Host(Site site)
        {
            var host = UIDialog.AsyncDialog(this.Context, "Setting", g =>
            {
                var from = new Web.UIFormDialog() { Title = "应用域名" };
                from.AddText("域名", "Setting", String.Empty);
                from.AddRadio("支持协议", "Scheme").Put("Http", "1", true).Put("Https", "2", true);//.Put("Http和Https", "0", true);
                from.AddCheckBox("", "IsAuth", "no").Put("身份模式域名", "Yes");
                from.Submit("确认", "Site.Config");
                return from;
            });
            if (System.Text.RegularExpressions.Regex.IsMatch(host, @"^([a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?\.)+[a-z0-9]{1,6}$") == false)
            {
                this.Prompt("域名格式不正确");
            }
            var h = DataFactory.Instance().Host(host);
            if (h != null && String.Equals(h.Root, site.Root) == false)
            {
                this.Prompt("此域名已经绑定其他应用");
            }
            DataFactory.Instance().Put(new SiteHost
            {
                Root = site.Root,
                Scheme = UMC.Data.Utility.IntParse(this.AsyncDialog("Scheme", "0"), 0),
                IsAuthModel = this.AsyncDialog("IsAuth", "0").Contains("Yes"),
                Host = host,
            }); ;

            this.Context.Send("Site.Config", true);
        }

        private void Domain(Site site)
        {
            var Setting = this.AsyncDialog("Setting", g =>
            {

                var from2 = new UIFormDialog() { Title = "负载网址" };

                from2.AddText("负载域名", "Host", site.Host).PlaceHolder("默认取反代的域名").NotRequired();
                from2.AddRadio("均衡策略", "SLB")
                .Put("随机", "0", (site.SLB ?? 0) == 0)
                .Put("IP", "1", site.SLB == 1)
                .Put("Cookie", "2", site.SLB == 2);

                from2.AddTextarea("网址", "Domain", site.Domain).Put("Rows", 10);//.PlaceHolder("服务网址");

                from2.AddFooter("后缀[0-9]表示负载均衡权重参数，后缀@user表示用户灰度");

                from2.Submit("确认", "Site.Config");
                return from2;
            });


            var Domain = Setting["Domain"];
            var doms = Domain.Split(',', '\n');
            foreach (var v in doms)
            {
                if (v.StartsWith("http://") == false && v.StartsWith("https://") == false && v.StartsWith("file://") == false && v.StartsWith("dns://") == false && v.StartsWith("unix://") == false)
                {
                    this.Prompt("格式不正确，请输入正确的网址");
                }
            }
            var host = Setting["Host"] ?? String.Empty;
            if (String.IsNullOrEmpty(host) == false)
            {
                switch (host)
                {
                    case "*":
                        break;
                    case "none":
                        host = String.Empty;
                        break;
                    default:
                        if (System.Text.RegularExpressions.Regex.IsMatch(host, @"^([a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?\.)+[a-z0-9]{1,6}$") == false)
                        {
                            this.Prompt("负载域名格式不正确");
                        }

                        break;
                }
            }
            DataFactory.Instance().Put(new Site
            {
                Root = site.Root,
                Domain = Domain,
                Host = host,
                SLB = UMC.Data.Utility.IntParse(Setting["SLB"], 0)
            }); ;
            this.Context.Send("Site.Config", true);
        }
        private void LogConf(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "日志参数" };
                from.AddTextarea("参数", "LogConf", site.LogConf).PlaceHolder("日志参数").Put("Rows", 10).NotRequired();
                from.AddFooter("默认获取Cookie值,以“:”开始表示获取请求Header值,以“:”结尾表示获取响应的Header值,多项用换行分割");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["LogConf"] ?? String.Empty;

            DataFactory.Instance().Put(new Site { Root = site.Root, LogConf = Key });

            this.Context.Send("Site.Config", true);
        }
        private void RedirectPath(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "切换地址" };
                from.AddTextarea("切换地址", "RedirectPath", site.RedirectPath).PlaceHolder("路径配置格式").Put("Rows", 6).NotRequired();

                from.AddFooter("多项用换行、空格或逗号分割，支持“*”前后取配");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["RedirectPath"] ?? String.Empty;
            if (String.Equals("none", Key))
            {
                Key = String.Empty;
            }

            DataFactory.Instance().Put(new Site { Root = site.Root, RedirectPath = Key });

            this.Context.Send("Site.Config", true);
        }
        private void AuthConf(Site site)
        {
            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "许可路径" };
                from.AddTextarea("许可路径", "AuthConf", site.AuthConf).PlaceHolder("路径配置格式").Put("Rows", 6).NotRequired();

                from.AddFooter("多项用换行、空格或逗号分割，支持“*”前后取配,当单行只有“*”，则表示只启用应用代理转发，关闭网关登录功能");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["AuthConf"] ?? String.Empty;
            if (String.Equals("none", Key))
            {
                Key = String.Empty;
            }

            DataFactory.Instance().Put(new Site { Root = site.Root, AuthConf = Key });

            this.Context.Send("Site.Config", true);
        }
        private void AppendJSConf(Site site)
        {

            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "追加脚本的路径" };

                from.AddTextarea("页面路径", "AppendJSConf", site.AppendJSConf).PlaceHolder("路径配置格式").Put("Rows", 6).NotRequired();

                from.AddFooter("多项用换行、空格或逗号分割");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["AppendJSConf"] ?? String.Empty;

            DataFactory.Instance().Put(new Site { Root = site.Root, AppendJSConf = Key });

            this.Context.Send("Site.Config", true);
        }
        private void ImagesConf(Site site)
        {

            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "处理图片路径" };

                from.AddTextarea("触发路径", "ImagesConf", site.ImagesConf).PlaceHolder("路径配置格式").Put("Rows", 6).NotRequired();

                from.AddFooter("多项用换行、空格或逗号分割");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["ImagesConf"] ?? String.Empty;

            DataFactory.Instance().Put(new Site { Root = site.Root, ImagesConf = Key });

            this.Context.Send("Site.Config", true);
        }

        private void StaticConf(Site site)
        {

            var config = this.AsyncDialog("Config", g =>
            {
                var from = new Web.UIFormDialog() { Title = "动静分离" };
                from.AddTextarea("分离路径", "StaticConf", site.StaticConf).Put("Rows", 10).PlaceHolder("只有路径，则表示清除默认分离的配置").NotRequired();

                from.AddFooter("默认对文件名为gif、ico、svg、bmp、png、jpg、jpeg、css、less、sass、scss、js、webp、jsx、coffee、ts、ttf、woff、woff2、wasm进行静态分离，分离参数all、user、 one、[num]");
                from.Submit("确认", "Site.Config");
                return from;
            });
            var Key = config["StaticConf"] ?? String.Empty;


            DataFactory.Instance().Put(new Site { Root = site.Root, StaticConf = Key });

            this.Context.Send("Site.Config", true);
        }

    }
}