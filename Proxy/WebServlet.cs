﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using UMC.Data;
using UMC.Net;
using UMC.Security;
using UMC.Web;
using System.IO;
using System.Collections;
using UMC.Proxy.Entities;
using System.Net;
using System.Text.RegularExpressions;


namespace UMC.Proxy
{
    public class WebServlet : UMC.Web.WebServlet
    {
        void SiteSsl(Net.NetContext context)
        {
            var host = context.Url.Host;
            switch (context.HttpMethod)
            {
                case "POST":
                    HttpMimeServier.SignSSL(host);
                    break;
            }

            context.Token = this.AccessToken(context);
            if (context.Token.IsInRole(UMC.Security.AccessToken.AdminRole) == false)
            {
                var seesionKey = UMC.Data.Utility.Guid(context.Token.Device.Value);
                var url = new Uri(AuthDomain(context), $"/Unauthorized?oauth_callback={Uri.EscapeDataString(context.Url.AbsoluteUri)}&transfer={seesionKey}").AbsoluteUri;
                Error(context, "安全防护", $"签发证书需要管理员权限，<a href=\"{url}\">请登录</a>");
            }
            else
            {
                Error(context, "签发证书", "Apiumc域名证书服务<br/><br/>建议：请在云桌面->应用设置->网关服务中集中管理证书", $"$.UI.Command('Proxy','Server',{{Model:'CSR',Domain:'{host}'}})");
            }
        }
        void Unauthorized(Net.NetContext context)
        {

            if (String.IsNullOrEmpty(context.QueryString["oauth_callback"]))
            {
                var reDomain = AuthDomain(context);
                if (String.Equals(context.Url.Host, reDomain.Host) == false)
                {
                    context.Redirect(new Uri(reDomain, $"/Unauthorized?transfer={GetCookie(context)}&oauth_callback={Uri.EscapeDataString(context.Url.AbsoluteUri)}").AbsoluteUri);
                }
                else
                {
                    context.Redirect(new Uri(reDomain, "/Unauthorized?oauth_callback=/").AbsoluteUri);
                }
            }
            else
            {
                Unauthorized(context, context.QueryString["oauth_callback"]);
            }
        }
        void Close(Net.NetContext context)
        {
            context.StatusCode = 403;

            HtmlResource(context, "close");
        }
        void HtmlResource(Net.NetContext context, string key)
        {
            context.ContentType = "text/html";
            using (System.IO.Stream stream = typeof(WebServlet).Assembly
                               .GetManifestResourceStream($"UMC.Proxy.Resources.{key}.html"))
            {

                var appId = UMC.Data.WebResource.Instance().Provider["appId"];
                var sb = new System.Text.StringBuilder();
                sb.AppendLine();
                sb.Append("<!--Version:");
                sb.Append(APIProxy.Version);
                if (!String.IsNullOrEmpty(appId))
                {
                    sb.Append("|");
                    sb.Append(appId);
                }
                sb.AppendLine("-->");
                var bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                context.ContentLength = stream.Length + bytes.Length;
                stream.CopyTo(context.OutputStream);
                context.OutputStream.Write(bytes, 0, bytes.Length);
            }
        }
        void NotSupport(Net.NetContext context)
        {
            context.StatusCode = 401;
            HtmlResource(context, "Auth.nosupport");
        }

        void Auth(Net.NetContext context)
        {
            switch (context.HttpMethod)
            {
                case "POST":
                    context.ReadAsForm(ns =>
                    {
                        var sign = ns["umc-request-sign"];
                        var appName = ns["umc-request-app"];
                        ns.Remove("umc-request-sign");
                        if (String.IsNullOrEmpty(sign))
                        {
                            context.StatusCode = 403;

                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", "缺少umc-request-sign参数"), context.Output);
                            return;
                        }
                        if (String.IsNullOrEmpty(appName))
                        {
                            context.StatusCode = 403;

                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", "缺少umc-request-app参数"), context.Output);
                            return;

                        }
                        var site = DataFactory.Instance().Site(appName);
                        if (site == null)
                        {
                            context.StatusCode = 403;

                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", $"未找到{appName}应用"), context.Output);
                            return;

                        }
                        if (String.IsNullOrEmpty(site.AppSecret))
                        {
                            context.StatusCode = 403;

                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", $"{site.Caption}未设置AppSecret"), context.Output);
                            return;

                        }
                        if (String.Equals(Utility.Sign(ns, site.AppSecret), sign, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var time = UMC.Data.Utility.IntParse(ns["umc-request-time"], 0);
                            if (Math.Abs(UMC.Data.Utility.TimeSpan() - time) > 300)
                            {
                                context.StatusCode = 403;
                                UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", "umc-request-time 参数不符合规定"), context.Output);
                            }
                            else
                            {

                                var auth_code = ns["umc-request-code"];
                                if (String.IsNullOrEmpty(auth_code))
                                {
                                    if (((site.AppSecretAuth ?? 0) & 2) == 0)
                                    {
                                        context.StatusCode = 403;
                                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", $"{site.Caption}未开通模拟登录"), context.Output);
                                        return;
                                    }
                                    var name = ns["umc-request-user-name"];
                                    if (String.IsNullOrEmpty(name))
                                    {
                                        context.StatusCode = 403;
                                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", "缺少umc-request-user-name 参数"), context.Output);
                                        return;
                                    }
                                    var id = ns["umc-request-user-id"];
                                    var srole = ns["umc-request-user-role"];
                                    var alias = ns["umc-request-user-alias"];
                                    var sid = new Guid(UMC.Data.Utility.MD5("umc.api.auth", id, name, srole, alias, appName));
                                    var user = UMC.Security.Membership.Instance().Identity(name);
                                    if (user == null)
                                    {
                                        String[] roles = new string[0];
                                        if (String.IsNullOrEmpty(srole) == false)
                                        {
                                            roles = srole.Split(',').Where(r => String.Equals(r, UMC.Security.AccessToken.AdminRole) == false).ToArray();

                                        }
                                        var uid = Data.Utility.Guid(id) ?? Utility.Guid(name, true).Value;
                                        user = UMC.Security.Identity.Create(uid, name, alias ?? name, roles);
                                    }

                                    new Session<UMC.Security.AccessToken>(new UMC.Data.AccessToken(sid).Login(user, 30 * 60), UMC.Data.Utility.Guid(sid))
                                        .Commit(user.Id.Value, "umc.api.auth", false, $"{context.UserHostAddress}/{context.Server}");


                                    UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "success").Put("data", new WebMeta("device", UMC.Data.Utility.Guid(sid))), context.Output);
                                }
                                else
                                {
                                    var seesion = UMC.Data.DataFactory.Instance().Session(auth_code);
                                    if (seesion != null)
                                    {
                                        var Value = UMC.Data.JSON.Deserialize<UMC.Data.AccessToken>(seesion.Content);
                                        var user = Value.Identity();
                                        UMC.Data.DataFactory.Instance().Delete(seesion);
                                        var webMeta = new WebMeta();
                                        webMeta.Put("name", user.Name).Put("id", user.Id).Put("alias", user.Alias).Put("roles", user.Roles).Put("organizes", user.Organizes);

                                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("code", "success").Put("data", webMeta), context.Output);

                                    }
                                    else
                                    {
                                        context.StatusCode = 403;
                                        UMC.Data.JSON.Serialize(new WebMeta("code", "error").Put("msg", "code仅能使用一次"), context.Output);

                                    }
                                }
                            }
                        }
                        else
                        {
                            context.StatusCode = 403;
                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "签名不正确"), context.Output);
                        }
                        context.OutputFinish();
                    });
                    break;
                default:
                    var appid = context.QueryString.Get("appid");
                    var redirect_uri = context.QueryString.Get("redirect_uri");

                    if (String.IsNullOrEmpty(appid))
                    {
                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "缺少appid参数"), context.Output);
                        return;
                    }

                    var site = DataFactory.Instance().Site(appid);
                    if (site == null)
                    {
                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "不存在此App"), context.Output);
                        return;
                    }
                    if (String.IsNullOrEmpty(site.AppSecret))
                    {
                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", $"{site.Caption}未设置AppSecret"), context.Output);
                        return;

                    }
                    if (((site.AppSecretAuth ?? 0) & 1) == 0)
                    {
                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta("code", "error").Put("msg", $"{site.Caption}求未开通OAuth授权"), context.Output);
                        return;
                    }
                    if (String.IsNullOrEmpty(redirect_uri))
                    {
                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "缺少redirect_uri参数"), context.Output);
                        return;
                    }

                    context.Token = this.AccessToken(context);
                    if (context.Token.IsAuthenticated)
                    {
                        var seesionKey = Utility.MD5(context.Token.Device.Value);

                        var sesion = UMC.Data.DataFactory.Instance().Session(context.Token.Device.ToString());

                        if (sesion != null)
                        {
                            sesion.SessionKey = seesionKey;
                            UMC.Data.DataFactory.Instance().Put(sesion);
                            if (redirect_uri.Contains("?"))
                            {
                                context.Redirect($"{redirect_uri}&auth_code={seesionKey}");
                            }
                            else
                            {
                                context.Redirect($"{redirect_uri}?auth_code={seesionKey}");

                            }

                        }
                        else
                        {
                            context.StatusCode = 403;
                            UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "服务端错误，请联系管理员"), context.Output);

                        }
                    }
                    else
                    {
                        Unauthorized(context);
                    }

                    break;
            }

        }

        internal static Uri AuthDomain(Net.NetContext context)
        {
            var d = Data.WebResource.Instance();
            var Domain = d.WebDomain();

            if (String.Equals("localhost", Domain) == false)
            {
                var auth = d.Provider["auth"];
                if (String.IsNullOrEmpty(auth))
                {
                    return new Uri($"{context.Url.Scheme}://{Domain}");
                }
                else
                {
                    return new Uri($"{context.Url.Scheme}://{auth}.{Domain}");
                }
            }
            else
            {
                return new Uri(context.Url, "/");
            }
        }
        protected override UMC.Security.AccessToken AccessToken(NetContext context)
        {

            var sessionKeys = context.Cookies.GetValues(SessionCookieName);
            if ((sessionKeys?.Length ?? 0) == 0)
            {
                var deviceId = Data.Utility.Guid(NewCookie(context), true).Value;

                return new UMC.Data.AccessToken(deviceId).Login(new UMC.Security.Guest(deviceId), 0);
            }
            else
            {
                var deviceId = Data.Utility.Guid(sessionKeys[sessionKeys.Length - 1], true).Value;
                var session = new Session<Data.AccessToken>(deviceId.ToString());
                if (session.Value != null && session.Value.Device == deviceId)
                {
                    var auth = session.Value;

                    var passDate = Data.Utility.TimeSpan();
                    if (auth.Timeout > 0)
                    {
                        if (((auth.ActiveTime ?? 0) + auth.Timeout) < passDate)
                        {
                            var at = new UMC.Data.AccessToken(deviceId).Login(new UMC.Security.Guest(deviceId), 0);
                            at.Commit(context.UserHostAddress, context.Server);
                            context.Token = at;
                            return at;

                        }
                    }
                    if (auth.BiometricTime > 0)
                    {
                        if (auth.ActiveTime < passDate - 300)
                        {
                            if (auth.ActiveTime < passDate - 600)
                            {
                                auth.BiometricTime = 0;
                            }
                            auth.Commit(context.UserHostAddress, context.Server);
                        }
                    }
                    else if (auth.ActiveTime < passDate - 600)
                    {
                        auth.Commit(context.UserHostAddress, context.Server);
                    }

                    context.Token = auth;
                    return auth;
                }

                return new UMC.Data.AccessToken(deviceId).Login(new UMC.Security.Guest(deviceId), 0);  // Security.AccessToken.Create<Data.AccessToken>(new UMC.Security.Guest(deviceId), deviceId, 0); ;



            }
        }

        void Unauthorized(Net.NetContext context, string oauth_callback)
        {

            context.Token = this.AccessToken(context);

            var reDomain = AuthDomain(context);

            var transfer = context.QueryString.Get("transfer");
            if (context.Token.IsAuthenticated)
            {
                if (String.IsNullOrEmpty(transfer) == false && String.Equals(GetCookie(context), transfer) == false)
                {
                    var login = UMC.Data.Reflection.Configuration("account")?["login"];
                    var timeout = UMC.Data.Utility.IntParse(login?.Attributes?["timeout"], 3600);
                    new UMC.Data.AccessToken(Utility.Guid(transfer, true).Value).Login(context.Token.Identity(), timeout).Commit("Desktop", context.UserHostAddress, context.Server);
                }
                context.Redirect(oauth_callback);
                return;
            }
            var webr = UMC.Data.WebResource.Instance();


            if (String.IsNullOrEmpty(context.UserAgent) == false)
            {
                var ua = context.UserAgent.ToUpper();
                if (ua.Contains("UMC CLIENT"))
                {
                    context.StatusCode = 401;
                    context.AddHeader("Cache-Control", "no-store");
                    context.ContentType = "text/html; charset=UTF-8";
                    using (System.IO.Stream stream = typeof(WebServlet).Assembly
                                             .GetManifestResourceStream("UMC.Proxy.Resources.umc.html"))
                    {
                        context.ContentLength = stream.Length;
                        stream.CopyTo(context.OutputStream);

                    }

                }
                else if (ua.Contains("MICROMESSENGER") || ua.Contains("WXWORK"))
                {
                    var account = UMC.Data.Reflection.Configuration("account");
                    var appids = new List<String>();
                    var wkappids = new List<String>();
                    foreach (var p in account.Providers)
                    {

                        switch (p.Type)
                        {
                            case "weixin":
                                appids.Add(p.Name);
                                break;
                            case "wxwork":
                                wkappids.Add(p.Name);
                                break;
                        }
                    }
                    if (wkappids.Count > 0)
                    {
                        context.Token.Put("oauth_callback", oauth_callback).Commit(context.UserHostAddress, context.Server);
                        var wxP = account[wkappids[0]];
                        var agentid = wxP["agentid"];
                        var redirect_uri = Uri.EscapeDataString(new Uri(reDomain, $"/UMC/wxwork.html?appid={wxP.Name}").AbsoluteUri);
                        var urlStr = $"https://open.weixin.qq.com/connect/oauth2/authorize?appid={wxP.Name}&response_type=code&scope=snsapi_base&state={transfer}&redirect_uri={redirect_uri}#wechat_redirect";

                        if (String.IsNullOrEmpty(agentid) == false)
                        {
                            urlStr = $"https://open.weixin.qq.com/connect/oauth2/authorize?appid={wxP.Name}&response_type=code&agentid={agentid}&scope=snsapi_privateinfo&state={transfer}&redirect_uri={redirect_uri}#wechat_redirect";

                        }
                        context.Redirect(urlStr);
                        return;
                    }
                    else if (appids.Count > 0)
                    {
                        var redirect_uri = Uri.EscapeDataString(new Uri(reDomain, $"/UMC/weixin.html?appid={appids[0]}").AbsoluteUri);
                        context.Token.Put("oauth_callback", oauth_callback).Commit(context.UserHostAddress, context.Server);
                        context.Redirect($"https://open.weixin.qq.com/connect/oauth2/authorize?appid={appids[0]}&response_type=code&scope=snsapi_base&state={transfer}&redirect_uri={redirect_uri}#wechat_redirect");//, Uri.EscapeDataString(new Uri(reDomain,$"/weixin?appid={appids[0]}").AbsoluteUri), appids[0]));
                    }
                    else
                    {
                        Error(context, "微信使用提示", "缺少微信参数，请联系管理员");
                    }
                }
                else if (ua.Contains("DINGTALK"))
                {
                    var account = UMC.Data.Reflection.Configuration("account");
                    var appids = new List<String>();
                    foreach (var p in account.Providers)
                    {
                        if (String.Equals(p.Type, "dingtalk"))
                        {

                            appids.Add(p.Name);
                        }
                    }
                    if (appids.Count == 0)
                    {
                        Error(context, "钉钉使用提示", "缺少钉钉参数，请联系管理员");

                    }
                    else
                    {
                        context.StatusCode = 401;
                        context.AddHeader("Cache-Control", "no-store");
                        context.ContentType = "text/html; charset=UTF-8";
                        using (System.IO.Stream stream = typeof(WebServlet).Assembly//UMC.Proxy
                                                 .GetManifestResourceStream("UMC.Proxy.Resources.dingtalk.html"))
                        {
                            var str = new System.IO.StreamReader(stream).ReadToEnd();
                            var v = new System.Text.RegularExpressions.Regex("\\{(?<key>\\w+)\\}").Replace(str, g =>
                            {
                                var key = g.Groups["key"].Value.ToLower();
                                switch (key)
                                {
                                    case "appids":
                                        return UMC.Data.JSON.Serialize(appids);
                                }
                                return "";

                            });
                            context.Output.Write(v);

                        }

                    }
                }
                else
                {
                    context.StatusCode = 401;
                    context.AddHeader("Cache-Control", "no-store");
                    this.LocalResources(context, "/UI/Unauthorized.html", true);
                }


            }
        }
        public static void Error(Net.NetContext context, int statusCode, String title, String msg, String log)
        {
            context.StatusCode = statusCode;
            context.ContentType = "text/html; charset=UTF-8";
            context.AddHeader("Cache-Control", "no-store");
            using (System.IO.Stream stream = typeof(WebServlet).Assembly//UMC.Proxy
                                        .GetManifestResourceStream("UMC.Proxy.Resources.error.html"))
            {
                var str = new System.IO.StreamReader(stream).ReadToEnd();
                var v = new System.Text.RegularExpressions.Regex("\\{(?<key>\\w+)\\}").Replace(str, g =>
                {
                    var key = g.Groups["key"].Value.ToLower();
                    switch (key)
                    {
                        case "title":
                            return title;
                        case "msg":
                            return msg;
                        case "log":
                            return log;

                    }
                    return "";

                });
                context.Output.Write(v);


            }
        }
        public static void Error(Net.NetContext context, String title, String msg, String log)
        {
            Error(context, 401, title, msg, log);
        }
        public static void Error(Net.NetContext context, String title, String msg)
        {
            Error(context, 401, title, msg, "");
        }
        void Auth(Net.NetContext context, string wk)
        {
            context.ContentType = "text/html; charset=UTF-8";

            using (System.IO.Stream stream = typeof(WebServlet).Assembly//UMC.Proxy
                                                   .GetManifestResourceStream(String.Format("UMC.Proxy.Resources.{0}.html", wk)))
            {

                switch (wk)
                {
                    case "dingtalk":
                        {
                            var account = UMC.Data.Reflection.Configuration("account");
                            var appids = new List<String>();
                            foreach (var p in account.Providers)
                            {
                                if (String.Equals(p.Type, wk))
                                {
                                    appids.Add(p.Name);
                                }
                            }
                            var str = new System.IO.StreamReader(stream).ReadToEnd();
                            var v = new Regex("\\{(?<key>\\w+)\\}").Replace(str, g =>
                            {
                                var key = g.Groups["key"].Value.ToLower();
                                switch (key)
                                {
                                    case "appids":
                                        return UMC.Data.JSON.Serialize(appids);

                                }
                                return "";

                            });
                            context.Output.Write(v);
                        }
                        break;
                    default:
                        stream.CopyTo(context.OutputStream);
                        break;
                }

            }
        }
        String NewCookie(NetContext context)
        {
            var sessionKey = Utility.Guid(Guid.NewGuid());
            context.Cookies[SessionCookieName] = sessionKey;

            var cdmn = MainDomain;
            if (context.Url.Scheme == "https")
            {
                if (context.Url.Host.EndsWith(cdmn))
                {
                    context.AddHeader("Set-Cookie", $"device={sessionKey}; SameSite=None; Secure; Expires={DateTime.Now.AddYears(10):r}; HttpOnly; Domain={cdmn}; Path=/");
                }
                else
                {
                    context.AddHeader("Set-Cookie", $"device={sessionKey}; SameSite=None; Secure; HttpOnly; Path=/");
                }
            }
            else
            {
                if (context.Url.Host.EndsWith(cdmn))
                {
                    context.AddHeader("Set-Cookie", $"device={sessionKey}; Expires={DateTime.Now.AddYears(10):r}; HttpOnly; Domain={cdmn}; Path=/");
                }
                else
                {
                    context.AddHeader("Set-Cookie", $"device={sessionKey}; HttpOnly; Path=/");
                }
            }


            return sessionKey;


        }
        String GetCookie(NetContext context)
        {
            var cookies = context.Cookies.GetValues(SessionCookieName);
            if ((cookies?.Length ?? 0) == 0)
            {
                return NewCookie(context);
            }

            return cookies[cookies.Length - 1];
        }
        bool isLoginAPI = false;

        void Synch(NetContext context, String path)
        {
            var paths = new List<String>(path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));

            var sessionKey = GetCookie(context);

            var deviceId = Data.Utility.Guid(sessionKey, true).ToString();

            if (paths.Count > 1)
            {
                if (String.Equals(paths[1], context.Server))
                {
                    if (paths.Count > 2 && String.Equals(paths[2], "!"))
                    {
                        context.Redirect(context.RawUrl.Substring(context.RawUrl.IndexOf("/!/")));
                    }
                    else
                    {
                        context.Redirect("/UMC.Login");
                    }
                    return;
                }
                else if (paths.Count > 3 && String.Equals(paths[2], "!"))
                {
                    deviceId = paths[3];

                }

            }
            else
            {
                context.Redirect("/UMC.Login");
                return;
            }
            var p = UMC.Data.WebResource.Instance();
            var secret = p.Provider["appSecret"];
            var appId = p.Provider["appId"];
            var type = typeof(UMC.Data.Entities.Session).FullName;
            

            var webD = new Web.WebMeta();
            webD.Put("from", appId);
            webD.Put("time", UMC.Data.Utility.TimeSpan().ToString());
            webD.Put("type", type);
            webD.Put("sign", UMC.Data.Utility.Sign(webD, secret));
            webD.Put("value", new WebMeta().Put("SessionKey", deviceId));

            var sub = Net.NetSubscribe.Subscribes.FirstOrDefault(r => String.Equals(r.Key, paths[1]));
            if (sub == null)
            {
                context.Redirect("/UMC.Login");
                return;
            }

            var url = new Uri($"http://{sub.Address}:{sub.Port}/UMC.Synch");
            context.UseSynchronousIO(() =>
            {
            });
            url.WebRequest().Post(webD, xhr =>
            {
                xhr.ReadAsString(xhrStr =>
                {
                    if (xhr.StatusCode == HttpStatusCode.OK)
                    {
                        var seesion = JSON.Deserialize<UMC.Data.Entities.Session>(xhrStr);

                        UMC.Data.DataFactory.Instance().Put(seesion);
                        if (paths.Count > 2 && String.Equals(paths[2], "!"))
                        {
                            context.Redirect(context.RawUrl.Substring(context.RawUrl.IndexOf("/!/")));
                        }
                        else
                        {
                            context.Redirect("/UMC.Login");
                        }

                    }
                    else
                    {
                        context.Redirect("/UMC.Login");

                    }
                    context.OutputFinish();
                }, e =>
                {
                    context.Redirect("/UMC.Login");
                    context.OutputFinish();
                });
            });



        }
        void Transfer(SiteHost hostSite, UMC.Net.NetContext context, string rawUrl)
        {
            switch (hostSite.Scheme ?? 0)
            {
                case 0:
                    break;
                case 1:
                    if (String.Equals(context.Url.Scheme, "https") && context.Url.Port == 443)
                    {
                        context.Redirect($"http://{context.Url.Host}{context.RawUrl}");
                        return;
                    }
                    break;
                case 2:
                    if (String.Equals(context.Url.Scheme, "http") && context.Url.Port == 80)
                    {
                        context.Redirect($"https://{context.Url.Host}{context.RawUrl}");
                        return;
                    }
                    break;
            }
            var siteConfig = UMC.Proxy.DataFactory.Instance().SiteConfig(hostSite.Root);
            if (siteConfig.AllowAllPath == false)
            {
                switch (context.HttpMethod)
                {
                    case "GET":
                        var domain = Data.WebResource.Instance().WebDomain();

                        var hostModel = siteConfig.Site.HostModel ?? HostModel.None;
                        var union = ".";
                        var scheme = Data.WebResource.Instance().Provider["scheme"] ?? "http";

                        if (HttpProxy.CheckPath(context.RawUrl, out var _RePath, siteConfig.RedirectPath))
                        {
                            var mainKey = String.Format("SITE_JS_CONFIG_{0}{1}", hostSite.Root, HttpProxy.MD5(_RePath, "")).ToUpper();
                            var config = UMC.Data.DataFactory.Instance().Config(mainKey);
                            if (String.IsNullOrEmpty(config?.ConfValue) == false)
                            {
                                context.Token = this.AccessToken(context);

                                if (String.IsNullOrEmpty(context.Token.Username) || String.Equals(context.Token.Username, "?"))
                                {
                                    var seesionKey = UMC.Data.Utility.Guid(context.Token.Device.Value);
                                    var seesion = UMC.Data.DataFactory.Instance().Session(seesionKey);

                                    if (seesion != null)
                                    {
                                        var Value = UMC.Data.JSON.Deserialize<UMC.Data.AccessToken>(seesion.Content);
                                        var user = Value.Identity();
                                        UMC.Data.DataFactory.Instance().Delete(seesion);
                                        context.Token.Login(user, 30).Commit("Desktop", true, context.UserHostAddress, context.Server);

                                    }
                                    if (String.IsNullOrEmpty(context.Token.Username) || String.Equals(context.Token.Username, "?"))
                                    {
                                        context.Redirect(new Uri(AuthDomain(context), $"/Unauthorized?oauth_callback={Uri.EscapeDataString(context.Url.AbsoluteUri)}&transfer={seesionKey}").AbsoluteUri);
                                        return;
                                    }
                                }
                                var wr = WebTransfer(siteConfig, context, rawUrl);
                                context.UseSynchronousIO(() => { });

                                wr.Net(context, xhr =>
                                {
                                    if (xhr.StatusCode == HttpStatusCode.OK && xhr.ContentType?.StartsWith("text/html", StringComparison.CurrentCultureIgnoreCase) == true)
                                    {
                                        xhr.Header(context);
                                        xhr.ReadAsStream(content =>
                                       {
                                           using (var outStream = DataFactory.Instance().Compress(context.OutputStream, xhr.ContentEncoding))
                                           {
                                               using (var f = DataFactory.Instance().Decompress(content, xhr.ContentEncoding))
                                               {
                                                   f.CopyTo(outStream);
                                               };
                                               var writer = new UMC.Net.TextWriter(outStream.Write);
                                               writer.Write("<script id=\"Site\" root=\"");
                                               writer.Write(hostSite.Root);
                                               writer.WriteLine("\">");
                                               writer.WriteLine(config.ConfValue);
                                               writer.WriteLine("</script>");
                                               writer.Flush();
                                               writer.Dispose();
                                               outStream.Flush();
                                               content.Close();
                                               content.Dispose();
                                           }
                                           context.OutputFinish();
                                       }, context.Error);

                                    }
                                    else
                                    {
                                        xhr.Transfer(context);
                                    }
                                });
                                return;
                            }
                            else
                            {
                                context.Redirect($"{scheme}://{hostSite.Root}{union}{domain}{context.RawUrl}");
                                return;
                            }
                        }

                        switch (hostModel)
                        {
                            case HostModel.Disable:
                                context.Redirect($"{scheme}://{hostSite.Root}{union}{domain}{context.RawUrl}");
                                return;
                            case HostModel.Check:

                                if (String.IsNullOrEmpty(context.Headers.GetIgnore("accept-language")) == false
                                    && String.IsNullOrEmpty(context.Headers.GetIgnore("accept-encoding")) == false)
                                {
                                    context.Redirect($"{scheme}://{hostSite.Root}{union}{domain}{context.RawUrl}");
                                    return;
                                }
                                break;
                            case HostModel.Login:

                                if (HttpProxy.IsLoginPath(siteConfig, context.RawUrl))
                                {
                                    context.Redirect($"{scheme}://{hostSite.Root}{union}{domain}{context.RawUrl}");
                                    return;
                                }
                                break;
                            case HostModel.None:


                                break;
                        }
                        break;
                }
            }
            for (var i = 0; i < siteConfig.SubSite.Length; i++)
            {
                var p = siteConfig.SubSite[i];
                if (rawUrl.StartsWith(p.Key))
                {
                    var site2 = UMC.Proxy.DataFactory.Instance().SiteConfig(p.Value);
                    if (site2 != null)
                    {
                        siteConfig = site2;
                        if (p.IsDel)
                        {
                            rawUrl = rawUrl.Substring(p.Key.Length);
                            if (rawUrl.StartsWith('/') == false)
                            {
                                rawUrl = "/" + rawUrl;
                            }
                        }
                        break;
                    }
                }
            }

            var indexPath = rawUrl.IndexOf('?');
            var path = indexPath > 0 ? rawUrl.Substring(0, indexPath) : rawUrl;
            Transfer(siteConfig, context, rawUrl);

        }
        void Cert(NetContext context)
        {
            var secret = UMC.Data.WebResource.Instance().Provider["appSecret"];
            var webr2 = new Uri(APIProxy.Uri, "Certificater").WebRequest();
            Utility.Sign(webr2, secret);
            context.UseSynchronousIO(() => { });
            switch (context.HttpMethod)
            {
                case "GET":
                    var sign = context.QueryString.Get("sign");
                    if (String.IsNullOrEmpty(sign))
                    {
                        HtmlResource(context, "app");
                        context.OutputFinish();

                    }
                    else
                    {
                        var p = UMC.Data.WebResource.Instance();
                        String appId = p.Provider["appId"];

                        var time = context.QueryString.Get("time");
                        var nvs = new NameValueCollection();
                        nvs.Add("appId", appId);
                        nvs.Add("time", time);

                        var webMeta = new WebMeta();
                        context.ContentType = "text/plain;charset=utf-8";
                        if (String.Equals(sign, UMC.Data.Utility.Sign(nvs, secret)))
                        {
                            var Nonce = Utility.Guid(Guid.NewGuid());
                            nvs.Add("nonce", Nonce);
                            webMeta.Put("nonce", Nonce);
                            webMeta.Put("sign", UMC.Data.Utility.Sign(nvs, secret));
                            webMeta.Put("msg", "验证通过").Put("code", "success");
                        }
                        else
                        {
                            context.StatusCode = 405;
                            webMeta.Put("msg", "域名所有权签名验证不通过").Put("code", "error");
                        }
                        UMC.Data.JSON.Serialize(webMeta, context.Output);
                        context.OutputFinish();
                    }

                    return;
                case "PUT":
                    UMC.Data.JSON.Serialize(new WebMeta("appId", UMC.Data.WebResource.Instance().Provider["appId"], "domain", MainDomain), context.Output);
                    context.OutputFinish();
                    break;
                default:

                    webr2.Post(new WebMeta().Put("type", "cert", "domain", context.Url.Host), r =>
                    {
                        if (r.StatusCode == HttpStatusCode.OK)
                        {
                            Utility.Certificate(r);

                            context.Output.Write("OK");

                            context.OutputFinish();
                        }
                        else
                        {
                            r.Transfer(context);
                        }
                    });
                    break;
            }
        }
        protected override void IndexResource(NetContext context)
        {
            if (context.Url.AbsolutePath.EndsWith(".html"))
            {
                base.IndexResource(context);
            }
            else
            {
                HtmlResource(context, "desktop.ui");
            }
        }
        void WebSocket(NetContext context)
        {
            switch (context.HttpMethod)
            {
                case "GET":
                    var sessionKey = Utility.Guid(context.RawUrl.Substring(8), true).Value;
                    JSON.Serialize(new WebMeta().Put("online", UMC.Host.HttpWebSocket.Online(sessionKey)), context.Output);

                    break;
                default:

                    context.UseSynchronousIO(() => { });
                    context.ReadAsStream(ms =>
                    {
                        using (var rnew = new System.IO.StreamReader(ms))
                        {

                            var str = rnew.ReadToEnd();
                            UMC.Host.HttpWebSocket.Send(Utility.Guid(context.RawUrl.Substring(8), true).Value, str);

                        }
                        context.OutputFinish();


                    }, context.Error);
                    break;
            }

        }
        public override void ProcessRequest(NetContext context)
        {
            var Path = context.Url.AbsolutePath;

            var s = Path.IndexOf('/', 1);

            var pfxPath = s > 0 ? Path.Substring(1, s - 1) : Path.Substring(1);

            var rawUrl = context.RawUrl;
            switch (pfxPath)
            {
                case "UMC.WS":
                    WebSocket(context);
                    return;
                case "UMC.TEMP":
                    Temp(context);
                    return;
                case "UMC.SSL":
                    SiteSsl(context);
                    return;
                case "UMC.Core":
                    base.Process(context);
                    return;
                case "UMC.For":
                    Synch(context, Path);
                    return;
                case "UMC.Synch":
                    Synchronize(context);
                    return;
                case "UMC.UI":
                    {
                        var _Domain = Data.WebResource.Instance().WebDomain();
                        var lPath = "/" + Path.Substring(5);
                        if (String.IsNullOrEmpty(_Domain) == false && _Domain.StartsWith("/") == false)
                        {
                            var host2 = context.Url.Host;
                            if (host2.EndsWith(_Domain) && host2.Length > _Domain.Length)
                            {
                                var sroot = host2.Substring(0, host2.Length - _Domain.Length - 1);

                                var psite2 = UMC.Proxy.DataFactory.Instance().SiteConfig(sroot);
                                if (psite2 != null)
                                {
                                    context.Token = new UMC.Data.AccessToken(Guid.Empty).Login(new UMC.Security.Guest(Guid.Empty), 0);
                                    var httpProxy = new HttpProxy(psite2, context, 0, rawUrl, String.Empty);

                                    if (httpProxy.Domain == null)
                                    {
                                        LocalResources(context, lPath, true);
                                    }
                                    else
                                    {
                                        context.UseSynchronousIO(() => { });
                                        httpProxy.Domain.WebRequest(rawUrl).Get(r =>
                                         {
                                             if (r.StatusCode == HttpStatusCode.OK)
                                             {
                                                 r.Transfer(context);
                                             }
                                             else
                                             {
                                                 LocalResources(context, lPath, true);
                                             }

                                         });
                                    }
                                }
                                else
                                {
                                    LocalResources(context, lPath, true);
                                }
                            }
                            else
                            {
                                LocalResources(context, lPath, true);

                            }

                        }
                        else
                        {
                            LocalResources(context, lPath, true);
                        }
                    }
                    return;
                case "UMC.Home":
                    HtmlResource(context, "desktop.umc");
                    return;
                case "UMC.TOP":
                    context.Redirect(AuthDomain(context).AbsoluteUri);
                    return;
                case "UMC.SignOut":
                    this.AccessToken(context).SignOut().Commit(context.UserHostAddress, context.Server);
                    context.Redirect(AuthDomain(context).AbsoluteUri);
                    return;
                case "UMC.Reset":
                    var urlReferrer = context.UrlReferrer;
                    if (urlReferrer != null)
                    {
                        context.Redirect(new Uri(AuthDomain(context), String.Format("/Unauthorized?oauth_callback={0}", Uri.EscapeDataString(urlReferrer.AbsoluteUri))).AbsoluteUri);

                    }
                    else
                    {
                        context.Redirect(AuthDomain(context).AbsoluteUri);
                    }
                    return;
                case "UMC.Clear":
                    {
                        var hs = new List<String>(context.Url.Host.Split('.'));
                        while (hs.Count > 1)
                        {
                            context.AddHeader("Set-Cookie", $"device=; Expires={DateTime.Now.AddDays(-1):r}; HttpOnly; Domain={String.Join('.', hs.ToArray())}; Path=/");
                            hs.RemoveAt(0);
                        }
                        context.AddHeader("Set-Cookie", $"device=; Expires={DateTime.Now.AddDays(-1):r}; HttpOnly; Path=/");

                        context.ContentType = "text/plain; charset=utf-8";
                        for (var i = 0; i < context.Headers.Count; i++)
                        {
                            context.Output.WriteLine($"{context.Headers.GetKey(i)}:{context.Headers.Get(i)}");
                        }
                    }
                    return;
                case "UMC.Conf.js":
                    {

                        context.AddHeader("Cache-Control", "no-store");
                        var CookieValue = GetCookie(context);
                        context.ContentType = "text/javascript;charset=utf-8";
                        context.Output.Write("UMC.UI.Config({possrc:'/UMC.',posurl: '");
                        context.Output.Write("/UMC.Core/");
                        context.Output.Write(CookieValue);
                        context.Output.Write("'");
                        {
                            var _Domain = Data.WebResource.Instance().WebDomain();
                            if (String.Equals("localhost", _Domain) == false)
                            {
                                var host2 = context.Url.Host;
                                if (host2.EndsWith(_Domain) && host2.Length > _Domain.Length)
                                {
                                    context.Output.Write(",'domain':'{0}://{1}'", context.Url.Scheme, _Domain);
                                    var sroot = host2.Substring(0, host2.Length - _Domain.Length - 1);

                                    var psite2 = UMC.Proxy.DataFactory.Instance().SiteConfig(sroot);
                                    if (psite2 != null)
                                    {
                                        context.Output.Write(",'root':'{0}'", psite2.Site.Root);
                                        context.Output.Write(",'site':'{0}'", psite2.Site.SiteKey ?? 0);
                                        context.Output.Write(",'title':{0}", UMC.Data.JSON.Serialize(psite2.Caption));
                                    }
                                }
                                else
                                {

                                    context.Output.Write(",'domain':'{0}://{1}'", context.Url.Scheme, _Domain);

                                }
                            }
                        }
                        context.Output.WriteLine("});");
                        context.Output.WriteLine("UMC.Src = '/UMC.UI/';");

                    }
                    return;
                case "UMC.Conf":
                    {
                        if (Path.Length > 12)
                        {

                            var keyIndex2 = Path.IndexOf('/', 11);
                            if (keyIndex2 > 0)
                            {
                                var psite = UMC.Proxy.DataFactory.Instance().SiteConfig(Path.Substring(10, keyIndex2 - 10));
                                if (psite != null)
                                {
                                    context.AddHeader("Last-Modified", DateTime.Now.ToString("r"));
                                    context.AddHeader("Cache-Control", "max-age=1000");
                                    var pathkey = Path.Substring(Path.LastIndexOf('/') + 1);
                                    var extIndex = pathkey.LastIndexOf('.');
                                    if (extIndex > 0)
                                    {
                                        pathkey = pathkey.Substring(0, extIndex);
                                    }

                                    var mainKey = String.Format("SITE_JS_CONFIG_{0}{1}", psite.Root, pathkey).ToUpper();
                                    var config = UMC.Data.DataFactory.Instance().Config(mainKey);
                                    context.ContentType = "text/javascript; utf-8";
                                    context.Output.WriteLine();
                                    if (config != null)
                                    {
                                        context.Output.WriteLine(config.ConfValue);
                                    }
                                    context.Output.Flush();

                                }
                            }
                        }
                        else
                        {
                            goto case "UMC.Conf.js";
                        }
                    }
                    return;
                case "UMC.Cert":
                    Cert(context);
                    return;
                case ".well-known":
                    {
                        var p = UMC.Data.WebResource.Instance();
                        String appId = p.Provider["appId"];
                        var secret = p.Provider["appSecret"];
                        var dcvfile = UMC.Data.Reflection.ConfigPath($"Static\\well-known\\{context.Url.Host}.dcv");

                        var sign = context.QueryString.Get("sign");
                        if (String.IsNullOrEmpty(sign) == false)
                        {
                            var time = context.QueryString.Get("time");
                            var nvs = new NameValueCollection();
                            nvs.Add("appId", appId);
                            nvs.Add("time", time);

                            if (String.Equals(sign, UMC.Data.Utility.Sign(nvs, secret)))
                            {
                                Utility.Writer(dcvfile, UMC.Data.JSON.Serialize(new WebMeta().Put("ContentType", context.QueryString.Get("type")).Put("Path", context.Url.AbsolutePath).Put("Content", context.QueryString.Get("value"))), false);
                                UMC.Data.JSON.Serialize(new WebMeta("msg", "OK"), context.Output);
                                context.OutputFinish();
                                return;
                            }

                        }
                        else if (System.IO.File.Exists(dcvfile))
                        {
                            var dcv = JSON.Deserialize<WebMeta>(Utility.Reader(dcvfile));
                            if (String.Equals(dcv?.Get("Path"), context.Url.AbsolutePath))
                            {
                                context.ContentType = dcv["ContentType"] ?? "text/plain";
                                context.Output.Write(dcv.Get("Content"));
                                HttpMimeServier.Register(30, new SiteCertCheck(context.Url.Host, secret));
                                return;
                            }
                        }
                        else if (context.Url.AbsolutePath.StartsWith("/.well-known/acme-challenge/"))
                        {
                            var webr = Utility.Sign(new Uri(APIProxy.Uri, "Certificater").WebRequest(), secret).Post(new WebMeta().Put("type", "file", "file", context.Url.AbsolutePath, "domain", context.Url.Host));
                            if (webr.StatusCode == HttpStatusCode.OK)
                            {
                                webr.Transfer(context);

                                HttpMimeServier.Register(30, new SiteCertCheck(context.Url.Host, secret));
                                return;
                            }
                        }
                    }
                    break;
                case "UMC.css":
                case "UMC.js":
                    LocalResources(context, "/" + Path.Substring(5), true);
                    return;
                case "!":
                    var sessionKey = Utility.Guid(GetCookie(context), true).Value;

                    Path = Path.Substring(3);
                    var key = Path;
                    var keyIndex = Path.IndexOf('/');
                    if (keyIndex > 0)
                    {
                        key = Path.Substring(0, keyIndex);
                    }
                    var seesion = UMC.Data.DataFactory.Instance().Session(key);
                    if (seesion != null)
                    {
                        var Value = UMC.Data.JSON.Deserialize<UMC.Data.AccessToken>(seesion.Content);
                        if (Value != null)
                        {
                            var login = UMC.Data.Reflection.Configuration("account")?["login"];
                            var timeout = UMC.Data.Utility.IntParse(login?.Attributes?["timeout"], 3600);
                            new UMC.Data.AccessToken(sessionKey).Login(Value.Identity(), timeout).Commit("Desktop", context.UserHostAddress, context.Server);

                        }
                        UMC.Data.DataFactory.Instance().Delete(seesion);
                    }

                    context.Redirect(String.Format("{0}{1}", Path.Substring(key.Length), context.Url.Query));



                    return;
                case "UMC.CDN":
                    {
                        var keyIndex2 = Path.IndexOf('/', 10);
                        if (keyIndex2 > 0)
                        {
                            var psite = UMC.Proxy.DataFactory.Instance().SiteConfig(Path.Substring(9, keyIndex2 - 9));
                            if (psite != null)
                            {
                                var sUrlIndex = rawUrl.IndexOf('/', keyIndex2 + 2);
                                context.Token = new UMC.Data.AccessToken(Guid.Empty).Login(new UMC.Security.Guest(Guid.Empty), 0);
                                rawUrl = rawUrl.Substring(sUrlIndex);
                                if (rawUrl.StartsWith("/UMC.Image/"))
                                {
                                    keyIndex2 = rawUrl.IndexOf('/', 12);
                                    if (keyIndex2 > 0)
                                    {
                                        context.QueryString["umc-image"] = rawUrl.Substring(11, keyIndex2 - 11);
                                        rawUrl = rawUrl.Substring(keyIndex2);
                                    }
                                }
                                var httpProxy = new HttpProxy(psite, context, 0, rawUrl, String.Empty);
                                if (httpProxy.Domain == null)
                                {
                                    Close(context);
                                }
                                else
                                {

                                    httpProxy.ProcessRequest();
                                }
                            }
                            else
                            {
                                Close(context);
                            }

                        }
                        else
                        {
                            Close(context);
                        }
                    }
                    return;
                case "UMC.Login":
                    isLoginAPI = true;
                    break;
                case "UMC.Image":
                    var keyIndex3 = Path.IndexOf('/', 12);
                    if (keyIndex3 > 0)
                    {
                        context.QueryString["umc-image"] = Path.Substring(11, keyIndex3 - 11);

                        rawUrl = rawUrl.Substring(keyIndex3);
                        context.RewriteUrl(rawUrl);
                    }
                    break;

            }


            var host = context.Url.Host;

            SiteConfig siteConfig = null;
            var pfxDomain = String.Empty;
            var ishttps = IsHttps;
            if (host.Length > MainDomain.Length && host.EndsWith(MainDomain))
            {
                var rook = host.Substring(0, host.Length - MainDomain.Length - 1);
                var ls = rook.Split('.');
                if (ls.Length > 1)
                {
                    pfxDomain = rook.Substring(0, rook.Length - ls[ls.Length - 1].Length - 1);
                    siteConfig = UMC.Proxy.DataFactory.Instance().SiteConfig(ls[ls.Length - 1]);
                }
                else
                {

                    siteConfig = UMC.Proxy.DataFactory.Instance().SiteConfig(ls[0]);
                }
            }
            if (siteConfig == null)
            {

                var hostSite = DataFactory.Instance().Host(host);
                if (hostSite != null)
                {
                    if (hostSite.IsAuthModel == true)
                    {
                        if (hostSite.Scheme == 2)
                        {
                            ishttps = true;
                        }
                        siteConfig = UMC.Proxy.DataFactory.Instance().SiteConfig(hostSite.Root);

                    }
                    else
                    {
                        Transfer(hostSite, context, rawUrl);

                        return;
                    }
                }
            }
            if (ishttps && context.Url.Port == 80)
            {
                context.Redirect($"https://{host}{context.RawUrl}");
                return;
            }
            if (siteConfig != null)
            {
                Proxy(context, siteConfig, pfxDomain, rawUrl);
                return;
            }

            switch (context.HttpMethod)
            {
                case "GET":

                    if (Path.StartsWith("/log/"))
                    {
                        context.Token = this.AccessToken(context);
                        if (context.Token.IsInRole(UMC.Security.Membership.AdminRole) == false)
                        {
                            context.Redirect("/");
                            return;
                        }
                    }

                    break;
            }
            var file = Path == "/" ? "/index.html" : Path;

            var staticFile = UMC.Data.Reflection.ConfigPath($"Static{file}");

            if (System.IO.File.Exists(staticFile))
            {
                TransmitFile(context, staticFile, true);
                return;
            }
            switch (Path)
            {
                case "/Biometric":

                    context.Token = this.AccessToken(context);
                    if (context.Token.BiometricTime > 0)
                    {
                        context.Redirect(context.QueryString["oauth_callback"]);
                    }
                    else
                    {
                        this.LocalResources(context, "/UI/Biometric.html", true);
                    }
                    return;
                case "/notsupport":
                    NotSupport(context);
                    return;
                case "/Unauthorized":
                    Unauthorized(context);
                    return;
                case "/Auth":
                    Auth(context);
                    return;
                case "/weixin":
                case "/dingtalk":
                case "/wxwork":
                    Auth(context, Path.Substring(1));
                    return;
                case "/":
                case "/Desktop":
                    HtmlResource(context, "desktop");
                    return;
                case "/favicon.ico":
                    context.StatusCode = 200;
                    context.ContentType = "image/x-icon";
                    using (System.IO.Stream stream = typeof(WebServlet).Assembly
                                       .GetManifestResourceStream("UMC.Proxy.Resources.favicon.ico"))
                    {
                        context.ContentLength = stream.Length;
                        stream.CopyTo(context.OutputStream);
                    }
                    return;
                default:
                    switch (pfxPath)
                    {
                        case "Docs":
                            HtmlResource(context, "desktop.doc");
                            return;
                        case "Setting":
                            var siteKey = Path.IndexOf('/', pfxPath.Length);
                            if (siteKey > 0)
                            {
                                if (DataFactory.Instance().Site(UMC.Data.Utility.IntParse(Path.Substring(siteKey + 1), 0)) != null)
                                {
                                    HtmlResource(context, "desktop.site");
                                }
                                else
                                {
                                    HtmlResource(context, "desktop.umc");
                                }
                            }
                            else
                            {
                                HtmlResource(context, "desktop.umc");
                            }
                            return;
                        case "Desktop":
                            HtmlResource(context, "desktop.page");
                            return;
                        default:
                            base.ProcessRequest(context);
                            break;


                    }


                    break;
            }




        }
        public static void WebHeaderConf(NetHttpRequest webR, SiteConfig siteConfig, UMC.Net.NetContext context, string account)
        {
            if (siteConfig.HeaderConf.Count > 0)
            {
                var he = siteConfig.HeaderConf.GetEnumerator();
                while (he.MoveNext())
                {
                    var v = he.Current.Value;
                    switch (v)
                    {

                        case "ACCOUNT":
                            if (String.IsNullOrEmpty(account) == false)
                            {
                                webR.Headers[he.Current.Key] = account;
                            }
                            break;
                        case "ROLES":
                            if (context.Token != null)
                            {
                                webR.Headers[he.Current.Key] = context.Token.Roles;
                            }
                            break;
                        case "TOKEN":
                            if (context.Token != null)
                            {
                                webR.Headers[he.Current.Key] = context.Token.Device.ToString();
                            }
                            break;
                        case "USERID":
                            if (context.Token != null)
                            {
                                webR.Headers[he.Current.Key] = context.Token.UserId.ToString();
                            }
                            break;
                        case "USERNAME":
                            if (context.Token != null)
                            {
                                webR.Headers[he.Current.Key] = context.Token.Username;
                            }
                            break;
                        case "HOST":
                            webR.Headers[he.Current.Key] = context.Url.Authority;
                            break;
                        case "SCHEME":
                            webR.Headers[he.Current.Key] = context.Url.Scheme;
                            break;
                        case "ADDRESS":
                            webR.Headers[he.Current.Key] = context.UserHostAddress;//.Split('/')[0];
                            break;
                        default:
                            webR.Headers[he.Current.Key] = v;
                            break;
                    }
                }
            }
        }
        NetHttpRequest WebTransfer(SiteConfig siteConfig, UMC.Net.NetContext context, string rawUrl)
        {

            var d = siteConfig.Domains[HttpProxy.WeightUri(siteConfig, context)];
            var dIndex = d.IndexOf('*');
            var webR = new Uri(dIndex > 0 ? d.Replace("*", "www") : d).WebRequest(rawUrl);
            WebHeaderConf(webR, siteConfig, context, String.Empty);


            var Headers = context.Headers;
            for (var i = 0; i < Headers.Count; i++)
            {
                var k = Headers.GetKey(i);
                var v = Headers.Get(i);
                switch (k.ToLower())
                {
                    case "content-type":
                        webR.ContentType = v;
                        break;
                    case "content-length":
                    case "connection":
                    case "host":
                        break;
                    case "user-agent":
                        webR.UserAgent = v;
                        break;
                    default:
                        webR.Headers.Add(k, v);
                        break;
                }
            }

            var host2 = siteConfig.Site.Host;
            if (String.IsNullOrEmpty(host2) == false)
            {
                var port = webR.Address.Port;
                if (String.Equals(host2, "*"))
                {
                    host2 = context.Url.Authority;
                }
                else
                {
                    switch (port)
                    {
                        case 80:
                        case 443:
                            break;
                        default:
                            host2 = String.Format("{0}:{1}", host2, port);
                            break;
                    }
                    var Referer = webR.Headers[HttpRequestHeader.Referer];
                    if (String.IsNullOrEmpty(Referer) == false)
                    {
                        webR.Headers[HttpRequestHeader.Referer] = String.Format("{0}://{1}{2}", webR.Address.Scheme, host2, Referer.Substring(Referer.IndexOf('/', 8)));

                    }
                    var Origin = webR.Headers["Origin"];
                    if (String.IsNullOrEmpty(Origin) == false)
                    {
                        webR.Headers["Origin"] = String.Format("{0}://{1}/", webR.Address.Scheme, host2);
                    }
                }
                webR.Headers[System.Net.HttpRequestHeader.Host] = host2;
            }
            return webR;
        }
        protected override void TransmitFile(NetContext context, string file, bool isCache)
        {
            var lastIndex = file.LastIndexOf('.');
            if (lastIndex > -1)
            {
                var extName = file.Substring(lastIndex + 1).ToLower();

                switch (extName)
                {
                    case "ico":
                        extName = "x-icon";
                        goto case "png";
                    case "jpg":
                        extName = "jpeg";
                        goto case "png";
                    case "bmp":
                    case "gif":
                    case "webp":
                    case "jpeg":
                    case "png":
                        string cacheFile;
                        var version = String.Empty;
                        var ckey = context.QueryString.Get("umc-image");
                        bool _IsCheckLicense = false;
                        if (string.IsNullOrEmpty(ckey) && file.Contains("/BgSrc/"))
                        {
                            _IsCheckLicense = true;
                        }
                        if (context.CheckCache("UMC", version, out cacheFile))
                        {
                            context.OutputFinish();
                            return;
                        }
                        WebMeta ImageConfig;
                        var contentType = $"image/{extName}";

                        if (_IsCheckLicense)
                        {
                            ImageConfig = new WebMeta().Put("Format", "Optimal");

                            var etag = Utility.TimeSpan();
                            var sWriter = NetClient.MimeStream(cacheFile, contentType, etag);
                            using (var fileStream = System.IO.File.OpenRead(file))
                            {
                                SiteImage.Convert(fileStream, sWriter, ImageConfig, cacheFile);
                            }
                            sWriter.Flush();
                            sWriter.Position = 0;
                            context.OutputCache(sWriter);
                            context.OutputFinish();
                            return;
                        }
                        if (HttpProxy.TryImageConf(ckey, out ImageConfig))
                        {
                            var format = ImageConfig["Format"] ?? "Src";
                            if (String.Equals(format, "Src") == false)
                            {
                                contentType = "image/" + format;
                            }
                            var tempFile = System.IO.Path.GetTempFileName();
                            var etag = Utility.TimeSpan();
                            using (var sWriter = NetClient.MimeStream(tempFile, contentType, etag))
                            {
                                using (var fileStream = System.IO.File.OpenRead(file))
                                {
                                    SiteImage.Convert(fileStream, sWriter, ImageConfig, cacheFile);
                                }
                                sWriter.Flush();
                                sWriter.Close();
                            }
                            Utility.Move(tempFile, cacheFile);

                            using (var fileStream = System.IO.File.OpenRead(cacheFile))
                            {
                                context.OutputCache(fileStream);
                            }
                            context.OutputFinish();
                            return;
                        }
                        break;
                }
            }

            base.TransmitFile(context, file, isCache);
        }
        void StaticFile(SiteConfig siteConfig, string dir, Net.NetContext context, string rawUrl)
        {
            var path = Uri.UnescapeDataString((rawUrl ?? context.Url.AbsolutePath).Split("?")[0]);
            var file = UMC.Data.Utility.FilePath(dir + path);

            switch (context.HttpMethod)
            {
                case "GET":
                    var lastIndex = file.LastIndexOf('.');
                    var extName = "html";
                    if (lastIndex > -1)
                    {
                        extName = file.Substring(lastIndex + 1).ToLower();

                    }
                    if (System.IO.File.Exists(file))
                    {

                        switch (extName)
                        {
                            case "ico":
                                extName = "x-icon";
                                goto case "png";
                            case "jpg":
                                extName = "jpeg";
                                goto case "png";
                            case "bmp":
                            case "gif":
                            case "webp":
                            case "jpeg":
                            case "png":
                                string filename;
                                if (context.CheckCache(siteConfig.Root, siteConfig.Site.Version, out filename))
                                {
                                    return;
                                }
                                WebMeta ImageConfig;
                                var contentType = $"image/{extName}";

                                var ckey = context.QueryString.Get("umc-image");
                                if (String.IsNullOrEmpty(ckey))
                                {
                                    HttpProxy.CheckPath(context.Url.AbsolutePath, contentType, out ckey, siteConfig.ImagesConf);
                                }

                                if (HttpProxy.TryImageConfig(siteConfig.Site.Root, ckey, out ImageConfig))
                                {
                                    var format = ImageConfig["Format"] ?? "Src";
                                    if (String.Equals(format, "Src") == false)
                                    {
                                        contentType = "image/" + format;
                                    }
                                    var tempFile = System.IO.Path.GetTempFileName();
                                    var etag = Utility.TimeSpan();
                                    using (var sWriter = NetClient.MimeStream(tempFile, contentType, etag))
                                    {
                                        using (var fileStream = System.IO.File.OpenRead(file))
                                        {
                                            SiteImage.Convert(fileStream, sWriter, ImageConfig, filename);
                                        }
                                        sWriter.Flush();
                                        sWriter.Position = 0;
                                        context.OutputCache(sWriter);
                                        sWriter.Close();
                                    }
                                    Utility.Move(tempFile, filename);
                                    return;

                                }
                                break;
                        }
                        base.TransmitFile(context, file, true);
                    }
                    else if (dir.EndsWith('/'))
                    {
                        var dirInfo = new System.IO.DirectoryInfo(file);
                        if (dirInfo.Exists)
                        {
                            context.ContentType = "text/html";
                            using (System.IO.Stream stream = typeof(WebServlet).Assembly
                                                 .GetManifestResourceStream("UMC.Proxy.Resources.dir.html"))
                            {
                                stream.CopyTo(context.OutputStream);
                            }
                            var dirs = dirInfo.GetDirectories().OrderBy(r => r.Name);
                            foreach (var d in dirs)
                            {
                                context.Output.WriteLine($"<tr><td><a class=\"icon dir\" href=\"{d.Name}/\">{d.Name}</a></td><td></td><td>{d.LastWriteTimeUtc:G}</td></tr>");
                            }
                            var files = dirInfo.GetFiles().OrderBy(r => r.Name);
                            foreach (var d in files)
                            {
                                context.Output.WriteLine($"<tr><td><a class=\"icon file\" href=\"{d.Name}\">{d.Name}</a></td><td>{Utility.GetBitSize(d.Length)}</td><td>{d.LastWriteTimeUtc:G}</td></tr>");
                            }
                            context.Output.WriteLine("</tbody></table>");
                            context.Output.WriteLine("</body>");
                            context.Output.WriteLine("</html>");
                            context.Output.Flush();
                        }
                        else
                        {
                            NotFound(context, extName, dir);
                        }
                    }
                    else
                    {
                        if (path.IndexOf('.', path.LastIndexOf('/')) == -1)
                        {
                            var staticFile = UMC.Data.Utility.FilePath(file + "/index.html");

                            if (System.IO.File.Exists(staticFile))
                            {
                                TransmitFile(context, staticFile, true);
                                return;

                            }
                        }
                        NotFound(context, extName, dir);
                    }
                    context.OutputFinish();
                    break;
                case "PUT":

                    if (Data.Utility.CheckSign(context.Headers, "umc-", siteConfig.Site.AppSecret) == false)
                    {

                        var d = UMC.Data.Utility.Writer(file, false);

                        context.ReadAsData((b, i, c) =>
                        {
                            if (c == 0 && b.Length == 0)
                            {

                                d.Close();
                                if (i == 0)
                                {
                                    UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "文件已经写入"), context.Output);
                                }
                                else
                                {
                                    System.IO.File.Delete(file);

                                    UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "写入文件出错"), context.Output);
                                }
                                context.OutputFinish();
                            }
                            else
                            {
                                d.Write(b, i, c);
                            }
                        });

                    }
                    else
                    {

                        context.StatusCode = 403;
                        UMC.Data.JSON.Serialize(new UMC.Web.WebMeta().Put("msg", "签名不正确"), context.Output);

                        context.OutputFinish();
                    }
                    break;
            }

        }
        static string _MainDomain;
        public static string MainDomain
        {
            get
            {
                if (String.IsNullOrEmpty(_MainDomain))
                {
                    _MainDomain = UMC.Data.WebResource.Instance().Provider["domain"] ?? "localhost";
                }
                return _MainDomain;
            }
            set
            {
                _MainDomain = value;
            }
        }
        void Transfer(SiteConfig siteConfig, UMC.Net.NetContext context, string RawUrl)
        {

            if (context.IsWebSocket)
            {
                context.Tag = WebTransfer(siteConfig, context, RawUrl);
                return;
            }
            var startTime = UMC.Data.Reflection.TimeSpanMilli(DateTime.Now);
            NameValueCollection resHeaders = null;
            String _attachmentFile = null;
            context.UseSynchronousIO(() =>
            {
                var duration = (int)(UMC.Data.Reflection.TimeSpanMilli(DateTime.Now) - startTime);

                HttpProxy.LogWrite(context, siteConfig, context.StatusCode, String.Format("{0} {1}", context.HttpMethod, context.Url.PathAndQuery), null, duration, resHeaders, _attachmentFile);

            });
            if (siteConfig.IsFile)
            {
                this.StaticFile(siteConfig, siteConfig.Domains[0].Substring(7), context, RawUrl);
                return;
            }
            switch (context.HttpMethod)
            {
                case "GET":
                    string cacheFile = String.Empty;
                    bool IsCache = false;
                    if (HttpProxy.CheckStaticPage(siteConfig, context.Url.AbsolutePath) == 0)
                    {
                        IsCache = true;
                        if (context.CheckCache(siteConfig.Root, siteConfig.Site.Version, out cacheFile))
                        {
                            context.OutputFinish();
                            return;
                        }
                    }

                    var wr = WebTransfer(siteConfig, context, RawUrl);
                    wr.Net(context, xhr =>
                    {

                        var contentType = (xhr.ContentType ?? String.Empty).Split(';')[0];
                        String ckey = null;
                        WebMeta ImageConfig = null;
                        if (contentType.StartsWith("image/") && contentType.Contains("svg") == false)
                        {
                            ckey = context.QueryString.Get("umc-image");
                            if (String.IsNullOrEmpty(ckey))
                            {
                                HttpProxy.CheckPath(context.Url.AbsolutePath, contentType, out ckey, siteConfig.ImagesConf);

                            }
                            if (HttpProxy.TryImageConfig(siteConfig.Site.Root, ckey, out ImageConfig))
                            {
                                var format = ImageConfig["Format"] ?? "Src";
                                if (String.Equals(format, "Src") == false)
                                {
                                    contentType = "image/" + format;
                                }
                                IsCache = true;
                            }
                        }
                        resHeaders = xhr.Headers;
                        if (xhr.StatusCode == HttpStatusCode.OK && IsCache)
                        {
                            var temp = Path.GetTempFileName();

                            if (ImageConfig != null)
                            {
                                xhr.ReadAsStream(ms =>
                                {
                                    var etag = Utility.TimeSpan();
                                    var outStream = NetClient.MimeStream(temp, contentType, etag);
                                    SiteImage.Convert(ms, outStream, ImageConfig, cacheFile);
                                    outStream.Flush();

                                    outStream.Position = 0;
                                    context.OutputCache(outStream);
                                    context.OutputFinish();
                                    outStream.Close();
                                    Utility.Move(temp, cacheFile);

                                }, err =>
                                {
                                    context.Error(xhr.Error);
                                });
                            }
                            else
                            {
                                var tempFile = File.Open(temp, FileMode.Create);

                                var tag = Utility.TimeSpan();
                                context.AddHeader("ETag", tag.ToString());

                                if (xhr.ContentLength > -1)
                                {
                                    context.ContentLength = xhr.ContentLength;
                                }
                                if (String.IsNullOrEmpty(xhr.ContentEncoding) == false)
                                {
                                    context.AddHeader("Content-Encoding", xhr.ContentEncoding);
                                }
                                context.ContentType = contentType;
                                xhr.ReadAsData((b, i, c) =>
                                {
                                    if (c == 0 && b.Length == 0)
                                    {
                                        if (i == -1)
                                        {
                                            tempFile.Close();
                                            File.Delete(temp);
                                            context.Error(xhr.Error);
                                        }
                                        else
                                        {
                                            context.OutputFinish();
                                            tempFile.Flush();
                                            tempFile.Position = 0;
                                            using (var tem = DataFactory.Instance().Decompress(tempFile, xhr.ContentEncoding))
                                            {
                                                var cacheStream = NetClient.MimeStream(cacheFile, contentType, tag);

                                                tem.CopyTo(cacheStream);
                                                tempFile.Close();
                                                cacheStream.Flush();
                                                cacheStream.Close();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        context.OutputStream.Write(b, i, c);
                                        tempFile.Write(b, i, c);
                                    }
                                });

                            }
                        }
                        else
                        {
                            _attachmentFile = xhr.AttachmentFile;
                            xhr.Transfer(context);

                        }
                    });

                    return;
            }

            var webR = WebTransfer(siteConfig, context, RawUrl);

            webR.Net(context, xhr =>
            {
                resHeaders = xhr.Headers;
                _attachmentFile = xhr.AttachmentFile;
                xhr.Transfer(context);
            });


        }
        void Synchronize(NetContext context)
        {
            context.UseSynchronousIO(() => { });
            switch (context.HttpMethod)
            {
                case "PUT":
                case "POST":
                    var buffer = System.Buffers.ArrayPool<byte>.Shared.Rent((int)context.ContentLength.Value);
                    var bufferSize = 0;
                    context.ReadAsData((b, i, c) =>
                    {
                        if (c == 0 && b.Length == 0)
                        {
                            try
                            {
                                if (i == -1)
                                {
                                    context.StatusCode = 405;
                                    context.ContentType = "text/plain; charset=utf-8";
                                    context.Output.Write("接收Body错误");
                                    context.OutputFinish();
                                }
                                else
                                {
                                    var value = System.Text.Encoding.UTF8.GetString(buffer, 0, bufferSize);
                                    var p = UMC.Data.WebResource.Instance();
                                    String appId = p.Provider["appId"];

                                    var hsh = JSON.Deserialize<Hashtable>(value);

                                    var time = hsh["time"] as string;
                                    var point = hsh["point"] as string;
                                    var nvs = new NameValueCollection();

                                    nvs.Add("from", appId);
                                    nvs.Add("time", time);
                                    nvs.Add("point", point);

                                    var type = hsh["type"] as string;
                                    var secret = p.Provider["appSecret"];
                                    if (String.IsNullOrEmpty(type) == false)
                                    {

                                        nvs.Add("type", type);

                                        if (String.Equals(hsh["sign"] as string, UMC.Data.Utility.Sign(nvs, secret)))
                                        {
                                            var v = UMC.Data.HotCache.Cache(type, hsh["value"] as Hashtable);
                                            if (v != null)
                                            {
                                                context.ContentType = "application/json; charset=utf-8";
                                                UMC.Data.JSON.Serialize(v, context.Output, "ts");
                                                return;
                                            }
                                            else
                                            {
                                                context.StatusCode = 404;
                                            }
                                        }
                                        else
                                        {
                                            context.StatusCode = 404;
                                        }
                                    }
                                    else
                                    {

                                        var webMeta = new WebMeta();
                                        if (String.Equals(hsh["sign"] as string, UMC.Data.Utility.Sign(nvs, secret)))
                                        {
                                            if (String.Equals(point, context.Server))
                                            {
                                                context.StatusCode = 405;
                                                webMeta.Put("msg", "不能设置自己为同步节点");
                                            }
                                            else
                                            {
                                                webMeta.Put("msg", "同步节点验证通过").Put("verify", true).Put("server", context.Server);
                                            }
                                        }
                                        else
                                        {
                                            context.StatusCode = 401;
                                            webMeta.Put("msg", "同步节点验证不通过");
                                        }
                                        UMC.Data.JSON.Serialize(webMeta, context.Output);
                                    }

                                    context.OutputFinish();

                                }
                            }
                            finally
                            {
                                System.Buffers.ArrayPool<byte>.Shared.Return(buffer);

                            }
                        }
                        else
                        {
                            Array.Copy(b, i, buffer, bufferSize, c);
                            bufferSize += c;
                        }
                    });
                    break;
                default:
                    context.StatusCode = 401;
                    context.OutputFinish();
                    break;
            }
        }
        public static bool IsHttps
        {
            get; set;
        }

        bool IsAllowPath(string path, string pfxDomain, SiteConfig psite)
        {
            var aPath = psite.AllowPath;

            if (String.IsNullOrEmpty(pfxDomain) == false && psite.AllowSubsPath.TryGetValue(pfxDomain, out aPath) == false)
            {
                aPath = psite.AllowPath;
            }
            foreach (var d in aPath)
            {
                if (path.StartsWith(d.StartPath) == false)
                {
                    continue;
                }
                if (path.EndsWith(d.EndPath))
                {
                    return true;
                }

            }
            return false;

        }

        void Proxy(UMC.Net.NetContext context, SiteConfig psite, String pfxDomain, string rawUrl)
        {
            for (var i = 0; i < psite.SubSite.Length; i++)
            {
                var p = psite.SubSite[i];
                if (rawUrl.StartsWith(p.Key))
                {
                    var site2 = UMC.Proxy.DataFactory.Instance().SiteConfig(p.Value);
                    if (site2 != null)
                    {
                        psite = site2;
                        if (p.IsDel)
                        {
                            rawUrl = rawUrl.Substring(p.Key.Length);
                            if (rawUrl.StartsWith('/') == false)
                            {
                                rawUrl = "/" + rawUrl;
                            }
                        }
                        break;
                    }
                }
            }

            if (psite.Site.Flag < 0 || psite.Domains.Length == 0)
            {
                Close(context);
                return;
            }


            var indexPath = rawUrl.IndexOf('?');
            var path = indexPath > 0 ? rawUrl.Substring(0, indexPath) : rawUrl;


            if (psite.AllowAllPath || String.Equals(context.HttpMethod, "OPTIONS") || IsAllowPath(path, pfxDomain, psite))
            {

                context.Token = new UMC.Data.AccessToken(Guid.Empty).Login(new UMC.Security.Guest(Guid.Empty), 0);// this.AccessToken(context);
                if (psite.IsFile)
                {
                    this.StaticFile(psite, psite.Domains[0].Substring(7), context, rawUrl);
                }
                else
                {
                    var httpProxy = new HttpProxy(psite, context, HttpProxy.CheckStaticPage(psite, path), rawUrl, pfxDomain);
                    if (context.IsWebSocket)
                    {
                        var webr = httpProxy.Reqesut(context.Transfer(httpProxy.Domain));
                        webr.RawUrl = httpProxy.RawUrl;
                        context.Tag = webr;

                    }
                    else
                    {
                        httpProxy.ProcessRequest();
                    }
                }
                return;

            }
            var IsAuth = false;
            var isBiometric = false;
            context.Token = this.AccessToken(context);

            var user = context.Token.Identity();
            switch (psite.Site.AuthType ?? Web.WebAuthType.User)
            {
                case Web.WebAuthType.Admin:
                    if (user.IsInRole(UMC.Security.Membership.AdminRole))
                    {
                        IsAuth = true;
                    }
                    else if (UMC.Data.DataFactory.Instance().Roles(user.Id.Value, psite.Site.SiteKey.Value).Contains(UMC.Security.Membership.AdminRole))
                    {
                        IsAuth = true;
                    }
                    break;
                default:
                case Web.WebAuthType.All:
                    IsAuth = true;
                    break;
                case Web.WebAuthType.UserCheck:
                    if (user.IsInRole(UMC.Security.Membership.UserRole))
                    {
                        if (AuthManager.IsAuthorization(user, 0, $"Desktop/{psite.Root}", out isBiometric))
                        {
                            IsAuth = true;
                        }
                    }
                    break;
                case Web.WebAuthType.User:

                    IsAuth = user.IsInRole(UMC.Security.Membership.UserRole);

                    break;
                case Web.WebAuthType.Check:

                    if (user.IsAuthenticated)
                    {
                        if (AuthManager.IsAuthorization(user, 0, $"Desktop/{psite.Root}", out isBiometric))
                        {
                            IsAuth = true;
                        }
                    }
                    break;
                case Web.WebAuthType.Guest:
                    IsAuth = user.IsAuthenticated;
                    break;
            }



            if (IsAuth)
            {
                var authBiometric = false;
                if (psite.Site.IsAuth == true)
                {
                    var authPath = rawUrl.Substring(1);

                    var lIndex = authPath.IndexOf('?');
                    if (lIndex > -1)
                    {
                        authPath = authPath.Substring(0, lIndex);
                    }
                    if (AuthManager.IsAuthorization(user, psite.Site.SiteKey.Value, authPath, out authBiometric) == false)
                    {
                        Error(context, "安全防护", "此资源受保护，请联系应用管理员");
                        return;
                    }

                }
                if (isBiometric || authBiometric)
                {
                    if (context.Token.BiometricTime == 0)
                    {
                        var seesionKey = UMC.Data.Utility.Guid(context.Token.Device.Value);
                        var url = new Uri(AuthDomain(context), $"/Biometric?oauth_callback={Uri.EscapeDataString(context.Url.AbsoluteUri)}&transfer={seesionKey}").AbsoluteUri;
                        context.Redirect(url);
                        return;
                    }
                }

                if (psite.IsFile)
                {
                    this.StaticFile(psite, psite.Domains[0].Substring(7), context, rawUrl);
                    return;
                }
                try
                {
                    var httpProxy = new HttpProxy(psite, context, HttpProxy.CheckStaticPage(psite, path), rawUrl, pfxDomain);

                    if (httpProxy.Domain == null)
                    {
                        Close(context);
                        return;
                    }
                    else if (isLoginAPI)
                    {
                        if (user.IsAuthenticated)
                        {
                            httpProxy.LoginRequest();

                        }
                        else
                        {
                            Unauthorized(context);
                        }
                        return;
                    }
                    switch (httpProxy.Domain.Scheme)
                    {
                        case "file":
                            break;
                        default:
                            if (context.IsWebSocket)
                            {
                                if (psite.Site.UserModel == Entities.UserModel.Bridge)
                                {
                                    httpProxy.AuthBridge();
                                }
                                var webr = httpProxy.Reqesut(context.Transfer(httpProxy.Domain));
                                webr.RawUrl = httpProxy.RawUrl;
                                // var getUrl = new Uri(httpProxy.Domain, httpProxy.RawUrl);
                                context.Tag = webr;//httpProxy.Reqesut(context.Transfer(getUrl));

                            }

                            else
                            {
                                httpProxy.ProcessRequest();
                            }
                            break;
                    }
                }
                catch (WebAbortException)
                {

                }
            }
            else if (user.IsAuthenticated)
            {
                Error(context, "安全审记", "你的权限不足或者登录过期，请 <a href=\"/UMC.Reset\">从新登录</a>");
            }
            else
            {
                Unauthorized(context);
            }

        }
    }

}
