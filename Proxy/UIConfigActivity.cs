﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UMC.Web;
using UMC.Data.Entities;
using UMC.Proxy.Entities;
using System.IO;
using UMC.Data;
using System.Text.RegularExpressions;

namespace UMC.Proxy.Activities
{
    [UMC.Web.Mapping("UI", "Config", Auth = WebAuthType.All)]
    public class UIConfigActivity : UMC.Web.WebActivity
    {
        public override void ProcessActivity(WebRequest request, WebResponse response)
        {
            var config = this.AsyncDialog("Type", "defalut");
            switch (config)
            {
                case "Bar":
                    break;
                case "Home":

                    var footBar = new List<WebMeta>();
                    footBar.Add(new WebMeta().Put("text", "云桌面", "key", "Pager", "cmd", "UI", "model", "Proxy", "icon", "\uf1b3", "RefreshEvent", "Proxy.Site"));
                    footBar.Add(new WebMeta("text", "发布应用", "icon", "\ue907").Put("max", true).Put("click", new UIClick("Proxy", "Site", "Create")));
                    footBar.Add(new WebMeta().Put("text", "我的", "key", "Pager", "cmd", "Self", "model", "Account", "icon", "", "RefreshEvent", "UI.Setting").Put("search", new WebMeta("Type", "Setting")));
                    this.Context.Reset(new WebMeta().Put("footBar", footBar));
                    break;
                default:
                    break;
            }
        }

    }

    [Mapping("System", "Scanning", Auth = WebAuthType.All, Desc = "移动扫描", Weight = 1)]
    class SiteScanningActivity : UMC.Web.Activities.SystemScanningActivity
    {
        protected override void Scanning(Uri url)
        {
            if (url.Host.Contains("apiumc.cn") || url.Host.Contains("apiumc.com"))
            {
                if (url.AbsolutePath.StartsWith("/UMC/Platform/Apper"))
                {
                    var Setup = System.Web.HttpUtility.ParseQueryString(url.Query).Get("Setup");
                    this.Context.Send(new UIClick("Proxy", "Setup").Send(new WebMeta("_", Setup)), true);


                }
            }
            base.Scanning(url);
        }
    }
}