﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using UMC.Net;

namespace UMC.Proxy
{

    class HttpBridgeMime : HttpMime
    {
        HttpBridgeClient client;
        public HttpBridgeMime(int pid, HttpBridgeClient client)
        {
            this.client = client;
            this.pid = pid;
        }
        internal int pid;
        public override string Host => "127.0.0.1";

        public override string RemoteIpAddress => "127.0.0.1";

        public override int Id => pid;

        bool isDispose = false;
        public override void Dispose()
        {
            if (isDispose == false)
            {
                isDispose = true;
                if (this.Request is IDisposable)
                {
                    ((IDisposable)this.Request).Dispose();
                }
                client.Remove(this.pid);
            }
        }
        public void WebSocket(byte[] buffer, int offset, int count)
        {
            try
            {
                this.Request?.Receive(buffer, offset, count);
            }
            catch
            {
                client.Write(this.pid, new byte[0], 0, 0);
                this.Dispose();
            }
        }



        public override void Write(byte[] buffer, int offset, int count)
        {
            if (count > 0)
            {

                client.Write(this.pid, buffer, offset, count);
            }
        }
    }
}

